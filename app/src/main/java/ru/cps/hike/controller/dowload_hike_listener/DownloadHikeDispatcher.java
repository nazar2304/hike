package ru.cps.hike.controller.dowload_hike_listener;

import android.util.Log;

import java.util.ArrayList;

import ru.cps.hike.App;
import ru.cps.hike.Constants;


public class DownloadHikeDispatcher {
    private static ArrayList<IDownloadStatusListener> listenerList = new ArrayList<>();

    public static void addListener(IDownloadStatusListener listener) {
        listenerList.add(listener);
    }

    public static void notifyAll(int resCode, String hikeId) {

        if (resCode == Constants.DOWNLOADING)
            App.downloadList.add(hikeId);
        else
            App.downloadList.remove(hikeId);

        for (IDownloadStatusListener l : listenerList) {
            if (l != null) l.onDownloadComplete(resCode, hikeId);
        }
    }
}
