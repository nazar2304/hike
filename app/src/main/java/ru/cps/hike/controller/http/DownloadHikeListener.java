package ru.cps.hike.controller.http;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.controller.dowload_hike_listener.DownloadControlListener;
import ru.cps.hike.controller.dowload_hike_listener.DownloadHikeDispatcher;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;
import ru.cps.hike.model.Point;
import ru.cps.hike.model.SimplePoint;
import ru.cps.hike.receivers.NotificationDismissedReceiver;

import static ru.cps.hike.Constants.HIKE_CHANEL_ID;


public class DownloadHikeListener implements View.OnClickListener {
    private static final int ERROR_Connection = 1;
    private static final int ERROR_SAVING = 2;
    private static final int zoomMin = 13;
    private static final int zoomMax = 18;
    private static int counter = 0;
    private String hikeId;
    private String hikeTitle;
    private String hikeImg;
    private String cityId;
    private Activity runingUi;
    private int notificationId;
    private int errorCode = -1;
    private int pointsCount;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder mBuilder;
    private boolean noExceptions = true;
    private ArrayList<SimplePoint> simplePoints = null;


    public DownloadHikeListener(String hikeId, String hikeTitle, String hikeImg, String cityId, Activity runingUi, int pointsCount) {
        this.hikeId = hikeId;
        this.hikeTitle = hikeTitle;
        this.hikeImg = hikeImg;
        this.notificationId = counter;
        this.runingUi = runingUi;
        this.cityId = cityId;
        this.pointsCount = pointsCount;
        counter++;
    }

    private static String readExportJsonFile(String hikeId) {
        String result = "";
        StringBuilder sb = new StringBuilder();
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return result;
        }
        File sdFile;
        sdFile = new File(FileTools.getFolder() + "/" + hikeId, "export.json");
        try {
            BufferedReader br = new BufferedReader(new FileReader(sdFile));
            while ((result = br.readLine()) != null) {
                sb.append(result).append("\n");
            }
        } catch (Exception ignored) {
        }
        return sb.toString();
    }

    @Override
    public void onClick(View view) {
        if (StringRequests.isOnline()) {

            initNotification();
            final DownloadHikeTask d = new DownloadHikeTask();
            d.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, hikeId);
        }
    }

    private void initNotification() {
        notificationManager = (NotificationManager) runingUi.getSystemService(Context.NOTIFICATION_SERVICE);
        String downloading = runingUi.getString(R.string.downloading);

        Bitmap img = ImageLoader.getInstance().loadImageSync(App.getApp().getResources().getString(R.string.general) + hikeImg);
        if (img == null) {
            img = BitmapFactory.decodeResource(App.getApp().getResources(), R.drawable.no_image);
        }
        mBuilder = new NotificationCompat.Builder(runingUi, HIKE_CHANEL_ID);

        Intent intent = new Intent(App.getApp(), NotificationDismissedReceiver.class);
        intent.putExtra("hike", hikeId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(App.getApp(), notificationId, intent, 0);

        Notification notification = mBuilder
                .setContentTitle(hikeTitle)
                .setContentText(downloading + hikeTitle)
                .setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(true)
                .setLargeIcon(img)
                .setDeleteIntent(pendingIntent)
                .setProgress(100, 0, false)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(notificationId, notification);


    }

    private void cancelNotification() {
        String ns = Context.NOTIFICATION_SERVICE;
        notificationManager = (NotificationManager) runingUi.getSystemService(ns);
        notificationManager.cancel(notificationId);
    }

    private void sendBroadcastDownloadStatus(int processId) {
        Intent intent = new Intent(Constants.BROADCAST_DOWNLOAD);
        intent.putExtra(Constants.DOWNLOAD, processId);
        intent.putExtra(Constants.HIKE_ID, hikeId);

        App.getApp().sendBroadcast(intent);
    }

    private void saveHikeIdAsDownloaded(String hikeId, int resCode) {

        if (resCode == Constants.DOWNLOAD_COMPLETE) {
            SaveInSharedPref.saveHikeId(hikeId);
            SharedPreferences prefs = App.getApp().getSharedPreferences(cityId, Context.MODE_PRIVATE);
            SharedPreferences.Editor ed = prefs.edit();

            ed.putInt(hikeId, resCode);
            ed.apply();
        }


    }

    private void removeHikeIdAsDownloaded(String hikeId) {
        SharedPreferences prefs = App.getApp().getSharedPreferences(cityId, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.remove(hikeId);
        ed.apply();
        SaveInSharedPref.removeHikeId(hikeId);
    }


    private ArrayList<SimplePoint> getPointArray(String hike) {
        String strJson = readExportJsonFile(hike);
        Hike h = new Gson().fromJson(strJson, Hike.class);
        myPointToSimplePoint(h.getPoints());
        return simplePoints;
    }

    private void myPointToSimplePoint(List<Point> points) {
        if (points != null) {
            simplePoints = new ArrayList<>();
            for (Point point : points) {
                simplePoints.add(new SimplePoint(point.getY(), point.getX()));
            }
        }
    }

    class DownloadHikeTask extends AsyncTask<String, Integer, Void> {
        int tileErrors = 0;
        long fullHikeLength = 0;
        long totalLength = 0;
        boolean cancel;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sendBroadcastDownloadStatus(Constants.DOWNLOADING);
            saveHikeIdAsDownloaded(hikeId, Constants.DOWNLOADING);
            DownloadHikeDispatcher.notifyAll(Constants.DOWNLOADING, hikeId);
            simplePoints = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(String... strings) {
            InputStream input = null;
            HttpURLConnection connection = null;
            int resCode;
            String link;

            try {
                link = StringRequests.downloadRequest(strings[0]);
                URL url = new URL(link);
                connection = (HttpURLConnection) url.openConnection();

                HttpURLConnection finalConnection = connection;
                DownloadControlListener.addListener((String hikeId) -> {
                    if (DownloadHikeListener.this.hikeId.equals(hikeId) || hikeId.equals("all")) {
                        finalConnection.disconnect();
                        cancel = true;
                        FileTools.removeHike(hikeId);
                        cancelNotification();
                    }

                });

                connection.connect();

                resCode = connection.getResponseCode();

                if (resCode == HttpURLConnection.HTTP_OK) {
                    input = new BufferedInputStream(connection.getInputStream());
                    noExceptions = downloadHike(input, strings[0], connection.getContentLength());
                    if (!cancel) {
                        tileErrors = startDowloadMap(getPointArray(strings[0]));
                    } else {
                        FileTools.removeHike(hikeId);
                    }
                } else {
                    noExceptions = false;
                    errorCode = ERROR_Connection;
                }
            } catch (MalformedURLException e) {
                noExceptions = false;
                errorCode = ERROR_Connection;
            } catch (IOException e) {
                noExceptions = false;
                if (errorCode == -1) {
                    errorCode = ERROR_SAVING;
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            new Handler().postDelayed(() -> {
                if (cancel)
                    return;
                mBuilder.setProgress(100, progress[0].intValue(), false);
                mBuilder.setContentText(progress[0].intValue() + " %");
                notificationManager.notify(notificationId, mBuilder.build());
            }, 200);


            super.onProgressUpdate(progress);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (noExceptions) {
                saveHikeIdAsDownloaded(hikeId, Constants.DOWNLOAD_COMPLETE);
                DownloadHikeDispatcher.notifyAll(Constants.DOWNLOAD_COMPLETE, hikeId);
                sendBroadcastDownloadStatus(Constants.DOWNLOAD_COMPLETE);
                if (tileErrors > 0) {
                    Toast.makeText(App.getApp(), R.string.tile_errors + " " + tileErrors, Toast.LENGTH_SHORT).show();
                }
            } else {
                FileTools.removeHike(hikeId);
                DownloadHikeDispatcher.notifyAll(Constants.DOWNLOAD_START, hikeId);
                removeHikeIdAsDownloaded(hikeId);
                sendBroadcastDownloadStatus(Constants.DOWNLOAD_ERROR);
                switch (errorCode) {
                    case ERROR_Connection:
                        Toast.makeText(App.getApp(), R.string.connection_exception, Toast.LENGTH_LONG).show();
                        break;
                    case ERROR_SAVING:
                        Toast.makeText(App.getApp(), R.string.save_exception, Toast.LENGTH_LONG).show();
                        break;
                }
            }
            cancel = true;
            notificationManager.cancel(notificationId);
        }

        private boolean downloadHike(InputStream input, String hikeid, int contentLength) {
            fullHikeLength = (contentLength + (pointsCount * 1024 * 1024));
            boolean result;
            ZipInputStream zis = new ZipInputStream(input);
            try {
                ZipEntry ze;
                int count = 0;
                long total = 0;
                byte[] buffer = new byte[8192];

                while ((ze = zis.getNextEntry()) != null) {

                    File targetDirectory = new File(FileTools.getFolder(), hikeid);
                    targetDirectory.mkdir();


                    File file = new File(targetDirectory, ze.getName());
                    File dir = ze.isDirectory() ? file : file.getParentFile();
                    if (!dir.isDirectory() && !dir.mkdirs())
                        throw new FileNotFoundException("Failed to ensure directory: " +
                                dir.getAbsolutePath());
                    if (ze.isDirectory())
                        continue;
                    try (FileOutputStream fout = new FileOutputStream(file)) {
                        while ((count = zis.read(buffer)) != -1) {
                            fout.write(buffer, 0, count);
                            total += count;
                        }
                        publishProgress((int) ((total * 100) / fullHikeLength));
                        totalLength = (int) total;
                    }
                }
                result = true;
            } catch (Exception e) {

                result = false;
            } finally {
                try {
                    zis.close();
                } catch (Exception ignored) {
                }
            }
            return result;
        }

        private int startDowloadMap(ArrayList<SimplePoint> pointArray) {
            int errors = 0;
            if (pointArray != null) {
                for (SimplePoint spoint : pointArray) {
                    SynkCacheManager cacheManager = new SynkCacheManager();
                    errors = errors + cacheManager.downloadAreaSync(spoint, zoomMin, zoomMax);
                    totalLength = totalLength + 1024 * 1024;
                    if (totalLength < fullHikeLength) {
                        publishProgress((int) ((totalLength * 100) / fullHikeLength));
                    }
                }
            }
            return errors;
        }
    }


}
