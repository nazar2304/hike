package ru.cps.hike.controller.http;

import android.content.Context;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import ru.cps.hike.App;
import ru.cps.hike.Constants;
import ru.cps.hike.R;


public class StringRequests {


    public static String pointsRequest(String hikeId) {
        return App.getApp().getResources().getString(R.string.general) + Constants.SHORT_POINTS_REQUEST + hikeId + Constants.DERMO_LANG + App.getCurrentLang();
    }

    static String downloadRequest(String hikeId) {
        return App.getApp().getResources().getString(R.string.general) + Constants.NEW_DOWNLOAD_RQUEST_BEGIN + hikeId + Constants.DOWNLOAD_RQUEST_END + Constants.DERMO_LANG_QU + App.getCurrentLang();
    }

    public static String getOneHikeRequest(String hikeId) {
        return App.getApp().getResources().getString(R.string.general) + Constants.NEW_DOWNLOAD_RQUEST_BEGIN + hikeId;
    }


    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) App.getApp().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
