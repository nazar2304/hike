package ru.cps.hike.controller.audio;

import java.util.ArrayList;


public class SeekBarPosDispatcher {
    private static ArrayList<ISeekBarPosListener> listenerList = new ArrayList<>();

    public static void addListener(ISeekBarPosListener listener) {
        listenerList.add(listener);
    }

    static void notifyAllUpdatePosition(int seekProgress, int seekMax) {
        for (ISeekBarPosListener l : listenerList) {
            if (l != null) {
                l.updatePosition(seekProgress, seekMax);
            }
        }
    }

    static void notifyAllHide() {
        for (ISeekBarPosListener l : listenerList) {
            if (l != null) {
                l.hideSeek();
            }
        }
    }

    static void notifyAllShow() {
        for (ISeekBarPosListener l : listenerList) {
            if (l != null) {
                l.showSeek();
            }
        }
    }
}
