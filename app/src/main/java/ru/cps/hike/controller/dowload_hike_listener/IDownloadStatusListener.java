package ru.cps.hike.controller.dowload_hike_listener;


public interface IDownloadStatusListener {
    void onDownloadComplete(int resCode, String hikeId);
}
