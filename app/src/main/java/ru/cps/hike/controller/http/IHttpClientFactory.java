package ru.cps.hike.controller.http;

import org.apache.http.client.HttpClient;


public interface IHttpClientFactory {
    HttpClient createHttpClient();
}