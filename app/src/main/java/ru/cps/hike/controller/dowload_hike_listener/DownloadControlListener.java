package ru.cps.hike.controller.dowload_hike_listener;

import android.util.Log;

import java.util.ArrayList;

public class DownloadControlListener {
    private static ArrayList<IControlDownloading> listenerList = new ArrayList<>();

    public static void addListener(IControlDownloading listener) {
        listenerList.add(listener);
    }

    public static void cancel(String hikeId) {
        for (IControlDownloading l : listenerList) {
            if (l != null) l.onCancelDownload(hikeId);
        }
    }

    public static void cancelAll() {
        for (IControlDownloading l : listenerList) {
            if (l != null) l.onCancelDownload("all");
        }
    }
}
