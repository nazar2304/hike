package ru.cps.hike.controller.http;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;


class HttpClientFactory {

    private static IHttpClientFactory mFactoryInstance = () -> {
        final DefaultHttpClient client = new DefaultHttpClient();
        client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "osmdroid");
        return client;
    };

    static HttpClient createHttpClient() {
        return mFactoryInstance.createHttpClient();
    }

}
