package ru.cps.hike.controller.http;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.osmdroid.tileprovider.MapTile;
import org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants;
import org.osmdroid.tileprovider.modules.TileWriter;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.util.StreamUtils;
import org.osmdroid.util.MyMath;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;

import ru.cps.hike.model.SimplePoint;

class SynkCacheManager {

    private final TileWriter mTileWriter;

    SynkCacheManager() {
        this.mTileWriter = new TileWriter();
    }

    private Point getMapTileFromCoordinates(double aLat, double aLon, int zoom) {
        int y = (int) Math.floor((1.0D - Math.log(Math.tan(aLat * 3.141592653589793D / 180.0D) + 1.0D / Math.cos(aLat * 3.141592653589793D / 180.0D)) / 3.141592653589793D) / 2.0D * (double) (1 << zoom));
        int x = (int) Math.floor((aLon + 180.0D) / 360.0D * (double) (1 << zoom));
        return new Point(x, y);
    }

    private File getFileName(ITileSource tileSource, MapTile tile) {
        return new File(OpenStreetMapTileProviderConstants.TILE_PATH_BASE, tileSource.getTileRelativeFilenameString(tile) + ".tile");
    }

    private boolean loadTile(OnlineTileSourceBase tileSource, MapTile tile) {
        File file = this.getFileName(tileSource, tile);
        if (file.exists()) {
            return true;
        } else {
            InputStream in = null;
            BufferedOutputStream out = null;

            try {
                String e = tileSource.getTileURLString(tile);
                HttpClient client = HttpClientFactory.createHttpClient();
                HttpGet head = new HttpGet(e);
                HttpResponse response = client.execute(head);
                StatusLine line = response.getStatusLine();
                if (line.getStatusCode() != 200) {
                    Log.w("BONUSPACK", "Problem downloading MapTile: " + tile + " HTTP response: " + line);
                    return false;
                }

                HttpEntity entity = response.getEntity();
                if (entity == null) {
                    Log.w("BONUSPACK", "No content downloading MapTile: " + tile);
                    return false;
                }

                in = entity.getContent();
                ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                out = new BufferedOutputStream(dataStream, 8192);
                StreamUtils.copy(in, out);
                out.flush();
                byte[] data = dataStream.toByteArray();
                ByteArrayInputStream byteStream = new ByteArrayInputStream(data);
                this.mTileWriter.saveFile(tileSource, tile, byteStream);
                byteStream.reset();
            } catch (UnknownHostException var21) {
                Log.w("BONUSPACK", "UnknownHostException downloading MapTile: " + tile + " : " + var21);
                return false;
            } catch (FileNotFoundException var22) {
                Log.w("BONUSPACK", "Tile not found: " + tile + " : " + var22);
                return false;
            } catch (IOException var23) {
                Log.w("BONUSPACK", "IOException downloading MapTile: " + tile + " : " + var23);
                return false;
            } catch (Throwable var24) {
                Log.e("BONUSPACK", "Error downloading MapTile: " + tile, var24);
                return false;
            } finally {
                StreamUtils.closeStream(in);
                StreamUtils.closeStream(out);
            }
            return true;
        }
    }





    int downloadAreaSync(SimplePoint simplePoint, int zoomMin, int zoomMax) {
        if (!(TileSourceFactory.MAPNIK instanceof OnlineTileSourceBase)) {
            Log.e("BONUSPACK", "TileSource is not an online tile source");
            return -1;
        } else {
            OnlineTileSourceBase tileSource = TileSourceFactory.MAPNIK;
            int errors = 0;
            int koefTitles;

            for (int zoomLevel = zoomMin + 1; zoomLevel <= zoomMax; zoomLevel += 2) {
                Point mLowerRight = SynkCacheManager.this.getMapTileFromCoordinates(simplePoint.getLatitudeY(), simplePoint.getLongitudeX(), zoomLevel);
                Point mUpperLeft = SynkCacheManager.this.getMapTileFromCoordinates(simplePoint.getLatitudeY(), simplePoint.getLongitudeX(), zoomLevel);
                int mapTileUpperBound = 1 << zoomLevel;

                if (zoomLevel > 16) {
                    koefTitles = 2;
                } else if (zoomLevel > 14) {
                    koefTitles = 1;
                } else {
                    koefTitles = 0;
                }

                for (int y = (mUpperLeft.y - koefTitles); y <= (mLowerRight.y + koefTitles); ++y) {
                    for (int x = (mUpperLeft.x - koefTitles); x <= (mLowerRight.x + koefTitles); ++x) {
                        int tileY = MyMath.mod(y, mapTileUpperBound);
                        int tileX = MyMath.mod(x, mapTileUpperBound);
                        MapTile tile = new MapTile(zoomLevel, tileX, tileY);
                        boolean ok = SynkCacheManager.this.loadTile(tileSource, tile);
                        if (!ok) {
                            ++errors;
                        }
                    }
                }
            }
            return errors;
        }
    }
}
