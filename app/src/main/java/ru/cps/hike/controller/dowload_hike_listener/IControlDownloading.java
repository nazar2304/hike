package ru.cps.hike.controller.dowload_hike_listener;

public interface IControlDownloading {
    void onCancelDownload(String hikeId);
}
