package ru.cps.hike.controller.files;


import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import ru.cps.hike.App;
import ru.cps.hike.Constants;
import ru.cps.hike.R;

public class FileTools {

    public static String getFolder() {
        File filedir = new File(App.getApp().getExternalCacheDir() + "/" + App.getApp().getResources().getString(R.string.default_folder));
        filedir.mkdirs();
        return filedir.getPath();
    }


    private static String getImageFolder() {
        File filedir = new File(getFolder() + "/" + Constants.IMAGE_FOLDER);
        filedir.mkdirs();
        return filedir.getPath();
    }

    public static void saveRespond(String respond, String name) throws IOException {
        if (respond != null) {
            File file = new File(getImageFolder(), name + App.getCurrentLang() + Constants.JSON_EXT);
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(respond);
            bw.flush();
            bw.close();
        }
    }

    public static String readRespond(String name) {
        String result = "";
        StringBuilder sb = new StringBuilder();
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return result;
        }

        File sdFile = new File(getImageFolder(), "/" + name + App.getCurrentLang() + Constants.JSON_EXT);
        try (BufferedReader br = new BufferedReader(new FileReader(sdFile))) {
            while ((result = br.readLine()) != null) {
                sb.append(result).append("\n");
            }
        } catch (IOException ignored) {
        }
        return sb.toString();

    }

    public static String readExportFile(String hikeId) {
        String result = "";
        StringBuilder sb = new StringBuilder();
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return result;
        }
        File sdFile;
        if (SaveInSharedPref.isHikeDownloaded(hikeId)) {
            sdFile = new File(getFolder() + "/" + hikeId, "export.json");
        } else {
            sdFile = new File(getFolder() + "/" + hikeId + App.getCurrentLang(), "export.wlk");
        }
        try {
            BufferedReader br = new BufferedReader(new FileReader(sdFile));
            while ((result = br.readLine()) != null) {
                sb.append(result).append("\n");
            }
        } catch (IOException ignored) {
        }
        return sb.toString();
    }


    public static String readExportFileMyHike(String hikeId) {
        String result = "";
        StringBuilder sb = new StringBuilder();
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return result;
        }
        File sdFile = new File(getFolder() + "/" + hikeId + App.getCurrentLang(), "export.wlk");
        try {
            BufferedReader br = new BufferedReader(new FileReader(sdFile));
            while ((result = br.readLine()) != null) {
                sb.append(result).append("\n");
            }
        } catch (IOException ignored) {
        }
        return sb.toString();
    }

    public static void savePoints(String respond, String folder) throws IOException {
        if (respond != null) {

            File file = new File(makeHikeDirectory(folder + App.getCurrentLang()), "points" + Constants.JSON_EXT);
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(respond);

            bw.flush();
            bw.close();
        }
    }

    public static void saveExport(String respond, String folder) throws IOException {
        if (respond != null) {
            File file = new File(makeHikeDirectory(folder + App.getCurrentLang()), "export" + Constants.JSON_EXT);
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(respond);

            bw.flush();
            bw.close();
        }
    }

    public static boolean checkIsSaveHike(String hikeId) {
        File sdFile = new File(getFolder() + "/" + hikeId, "export.json");
        return sdFile.exists();
    }

    public static void removeHike(String hikeId) {
        new File(getFolder() + "/" + hikeId, "export.json").delete();
    }

    public static String readPoints(String hikeId) {
        String result = "";
        StringBuilder sb = new StringBuilder();
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return result;
        }

        File sdFile = new File(getFolder() + "/" + hikeId + App.getCurrentLang(), "points.wlk");
        try {
            BufferedReader br = new BufferedReader(new FileReader(sdFile));
            while ((result = br.readLine()) != null) {
                sb.append(result).append("\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();

    }

    private static File makeHikeDirectory(String hikeId) {
        File dir = new File(FileTools.getFolder());
        File hike = new File(dir, hikeId);
        hike.mkdir();
        dir = hike;
        return dir;
    }
}
