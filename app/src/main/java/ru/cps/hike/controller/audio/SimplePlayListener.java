package ru.cps.hike.controller.audio;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import ru.cps.hike.App;
import ru.cps.hike.Constants;


public class SimplePlayListener implements View.OnClickListener {
    private String hikeId;
    private String audio;
    private String title;
    private String imageName;
    private int index;
    private boolean isDownloaded;

    private SimplePlayListener(String hikeId, String audio, String title, String imageName, int index, boolean isDownloaded) {
        this.hikeId = hikeId;
        this.audio = audio;
        this.title = title;
        this.imageName = imageName;
        this.isDownloaded = isDownloaded;
        this.index = index;
    }

    public static SimplePlayListener getInstance(String hikeId, String audio, String title, String imageName, int index, boolean isDownloaded) {
        return new SimplePlayListener(hikeId, audio, title, imageName, index, isDownloaded);
    }

    public void onClick(View view) {
        try {
            if (AudioService.player != null) {
                if (!Constants.playingHikeId.equals(hikeId)) {
                    App.getApp().stopService(new Intent(App.getApp(), AudioService.class));
                    startAudioService();
                } else {
                    if (index != Constants.playIndex) {
                        App.getApp().stopService(new Intent(App.getApp(), AudioService.class));
                        startAudioService();

                    } else {
                        try {
                            if (!AudioService.player.isPlaying()) {
                                AudioService.playMedia();
                            } else {
                                AudioService.pauseMedia();
                            }
                        } catch (IllegalStateException e) {
                            Log.e("mp3", Log.getStackTraceString(e));
                        }
                    }
                }
            } else {
                startAudioService();
            }
        } catch (Exception e) {
            Log.e("mp3", Log.getStackTraceString(e));
        }
    }

    private void startAudioService() {
        Intent intent = new Intent(App.getApp(), AudioService.class);
        intent.putExtra(Constants.ID, hikeId);
        intent.putExtra(Constants.AUDIO, audio);
        intent.putExtra(Constants.IMAGE, imageName);
        intent.putExtra(Constants.TITLE, title);
        intent.putExtra(Constants.INDEX, index);
        intent.putExtra(Constants.IS_DOWNLOADED, isDownloaded);
        App.getApp().startService(intent);
    }
}
