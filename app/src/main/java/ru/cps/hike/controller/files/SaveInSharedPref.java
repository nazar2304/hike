package ru.cps.hike.controller.files;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.cps.hike.App;
import ru.cps.hike.Constants;


public class SaveInSharedPref {

    public static void saveHikeId(String hikeId) {
        ArrayList<String> hikeNames = loadHikeIds();
        if (hikeNames == null) {
            hikeNames = new ArrayList<>();
        }
        if (!hikeNames.contains(hikeId)) {
            hikeNames.add(hikeId);
            SharedPreferences sp = App.getApp().getSharedPreferences("MAIN_PREFS" + App.getApp().getPackageName(), Context.MODE_PRIVATE);

            if (sp != null) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(hikeNames.toString());
                } catch (JSONException ignored) {
                }
                if (jsonArray != null) {
                    String jsonString = jsonArray.toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove(Constants.DOWNLOADED_HIKES_ARRAY).apply();
                    editor.putString(Constants.DOWNLOADED_HIKES_ARRAY, jsonString);
                    editor.commit();
                }

            }
        }
    }

    public static void removeHikeId(String hikeId) {
        ArrayList<String> hikeNames = loadHikeIds();
        if (hikeNames == null) {
            return;
        }
        if (hikeNames.contains(hikeId)) {
            hikeNames.remove(hikeId);
            SharedPreferences sp = App.getApp().getSharedPreferences("MAIN_PREFS" + App.getApp().getPackageName(), Context.MODE_PRIVATE);

            if (sp != null) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(hikeNames.toString());
                } catch (JSONException ignored) {
                }
                if (jsonArray != null) {
                    String jsonString = jsonArray.toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove(Constants.DOWNLOADED_HIKES_ARRAY).apply();
                    editor.putString(Constants.DOWNLOADED_HIKES_ARRAY, jsonString);
                    editor.commit();
                }
            }
        }
    }

    public static ArrayList<String> loadHikeIds() {
        ArrayList<String> output = new ArrayList<>();
        try {
            SharedPreferences pSharedPref = App.getApp().getSharedPreferences("MAIN_PREFS" + App.getApp().getPackageName(), Context.MODE_PRIVATE);

            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString(Constants.DOWNLOADED_HIKES_ARRAY, (new JSONObject()).toString());
                if (!jsonString.isEmpty()) {
                    JSONArray jsonArray = new JSONArray(jsonString);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        output.add(jsonArray.get(i).toString());
                    }
                }
            }
        } catch (Exception e) {
            return null;
        }
        return output;
    }

    public static boolean isHikeDownloaded(String hikeName) {
        ArrayList<String> hikeNames = loadHikeIds();
        if (hikeNames != null) {
            for (String hName : hikeNames) {
                if (hName.equals(hikeName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
