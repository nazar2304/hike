package ru.cps.hike.controller.audio;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.IOException;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.Constants;

import static ru.cps.hike.Constants.HIKE_CHANEL_ID;


public class AudioService extends Service implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
        MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener {

    private static final int NOTIFICATION_ID = 1;
    public static MediaPlayer player;
    private static String hikeId;
    private static boolean isDowloaded;
    private static String image;
    private static String title;
    private static int index;
    private final Handler handler = new Handler();
    private String trackId;
    private Intent bufferIntent;
    private boolean isPausedInCall = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;
    private BroadcastReceiver nextTrackReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
        }
    };
    private Runnable sendUpdatesToUI = new Runnable() {
        @Override
        public void run() {
            logMediaPosition();
            handler.postDelayed(this, 200);
        }
    };
    private BroadcastReceiver seekBarReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateSeekPos(intent);
        }
    };

    private static void stopMedia() {
        if (player.isPlaying()) {
            Constants.playerPaused = false;
            player.stop();
            cancelNotification();
        }
    }

    public static void pauseMedia() {
        if (player.isPlaying()) {
            Constants.playerPaused = true;
            sendBroadcastButtonStatus();
            player.pause();
            cancelNotification();
        }
    }

    public static void playMedia() {
        if (!player.isPlaying()) {
            Constants.playerPaused = false;
            sendBroadcastButtonStatus();
            player.start();
            initNotification();
            Constants.playIndex = index;
            Constants.playingHikeId = hikeId;
            SeekBarPosDispatcher.notifyAllShow();
        }
    }

    private static void initNotification() {

        NotificationManager notificationManager = getNotificationManager();

        Bitmap img;

        if (SaveInSharedPref.isHikeDownloaded(hikeId)) {
            String sb = "file:///" + FileTools.getFolder() + "/" + hikeId + "/" + new File(image).getName();
            img = ImageLoader.getInstance().loadImageSync(sb);
        } else {
            img = ImageLoader.getInstance().loadImageSync(App.getApp().getResources().getString(R.string.general) + image);
        }


        if (img == null) {
            img = BitmapFactory.decodeResource(App.getApp().getResources(), R.drawable.no_image);
        }
        Notification notification = new NotificationCompat.Builder(App.getApp(), HIKE_CHANEL_ID)
                .setContentTitle(title)
                .setContentText(App.getApp().getString(R.string.point_number) + (index + 1))
                .setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(true)
                .setLargeIcon(img).build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;


        notificationManager.notify(NOTIFICATION_ID, notification);

    }

    private static void cancelNotification() {
        NotificationManager notificationManager = getNotificationManager();
        notificationManager.cancel(NOTIFICATION_ID);

    }

    private static NotificationManager getNotificationManager() {
        return (NotificationManager) App.getApp().getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private static void sendBroadcastButtonStatus() {
        Intent intent = new Intent(Constants.BROADCAST_PLAYER_STATUS);
        intent.putExtra(Constants.AUDIO, true);
        App.getApp().sendBroadcast(intent);

    }

    public static int getCurrentPosition() {
        if (player != null) {
            return player.getCurrentPosition();
        }
        return 0;
    }

    public static int getMaxPosition() {
        if (player != null) {
            return player.getDuration();
        }
        return 0;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        registerReceiver(seekBarReceiver, new IntentFilter(Constants.BROADCAST_SEEKBAR));


        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (player != null) {
                            if (player.isPlaying()) {
                                pauseMedia();
                                isPausedInCall = true;
                            }
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        if (player != null) {
                            if (isPausedInCall) {
                                isPausedInCall = false;
                                playMedia();
                            }
                        }
                        break;
                }
            }
        };

        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        if (getDataFromIntent(intent)) {
            initPlayer();
            setupHandler();
        } else {
            Toast.makeText(App.getApp(), R.string.unable_to_play_audio, Toast.LENGTH_SHORT).show();
        }
        return START_STICKY;
    }

    private boolean getDataFromIntent(Intent intent) {
        boolean result = false;
        if (intent != null) {
            hikeId = intent.getStringExtra(Constants.ID);
            isDowloaded = SaveInSharedPref.isHikeDownloaded(hikeId);

            String trackFileName = intent.getStringExtra(Constants.AUDIO);
            trackId = trackFileName;

            if (trackFileName == null) {
                return false;
            }
            if (trackFileName.isEmpty() || trackFileName.equals("null")) {
                return false;
            }

            image = intent.getStringExtra(Constants.IMAGE);

            title = intent.getStringExtra(Constants.TITLE);
            index = intent.getIntExtra(Constants.INDEX, 0);
            result = true;
        }
        return result;
    }

    private void initPlayer() {

        if (player == null) {
            player = new MediaPlayer();
            player.setOnCompletionListener(this);
            player.setOnErrorListener(this);
            player.setOnPreparedListener(this);
            player.setOnSeekCompleteListener(this);
            player.reset();
        } else {
            if (player.isPlaying()) {
                player.stop();
                player.reset();
                cancelNotification();
            } else {
                player.release();
                player = new MediaPlayer();
                player.setOnCompletionListener(this);
                player.setOnErrorListener(this);
                player.setOnPreparedListener(this);
                player.setOnSeekCompleteListener(this);
                player.reset();
            }
        }
        String trackPath;

        if (hikeId != null) {
            if (!trackId.isEmpty()) {
                if (isDowloaded) {
                    trackPath = FileTools.getFolder() + "/" + hikeId + "/" + new File(trackId).getName();
                } else {
                    trackPath = App.getApp().getResources().getString(R.string.general) + trackId;
                }
                try {
                    sendBufferingBroadcast();
                    player.setDataSource(trackPath);
                    player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    player.setVolume(100, 100);
                    player.prepareAsync();


                } catch (IOException e) {
                    stopSelf();
                    Toast.makeText(App.getApp(), R.string.unable_to_play_audio, Toast.LENGTH_SHORT).show();
                    sendBufferedCompleteBroadcast();
                    SeekBarPosDispatcher.notifyAllHide();
                }
            } else {
                Toast.makeText(App.getApp(), R.string.unable_to_play_audio, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        stopSelf();
        sendBufferedCompleteBroadcast();
        SeekBarPosDispatcher.notifyAllHide();
        Toast.makeText(App.getApp(), R.string.unable_to_play_audio, Toast.LENGTH_SHORT).show();
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_IO:
                Log.e("mp3", "??????????????????????????????????????MediaPlayer.MEDIA_ERROR_IO");
                break;
            case MediaPlayer.MEDIA_ERROR_MALFORMED:
                Log.e("mp3", "??????????????????????????????????????MediaPlayer.MEDIA_ERROR_MALFORMED");
                break;
            case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                Log.e("mp3", "??????????????????????????????????????MediaPlayer.MEDIA_ERROR_TIMED_OUT");
                break;
            case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
                Log.e("mp3", "??????????????????????????????????????MediaPlayer.MEDIA_ERROR_UNSUPPORTED");
                break;

            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Log.e("mp3", "??????????????????????????????????????MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK");
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.e("mp3", "??????????????????????????????????????MediaPlayer.MEDIA_ERROR_SERVER_DIED");
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.e("mp3", "?????????????????????????????????????? MediaPlayer.MEDIA_ERROR_UNKNOWN");
                break;
        }

        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        SeekBarPosDispatcher.notifyAllUpdatePosition(0, mediaPlayer.getDuration());
        SeekBarPosDispatcher.notifyAllHide();
        stopMedia();
        stopSelf();
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        sendBufferedCompleteBroadcast();
        playMedia();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        bufferIntent = new Intent(Constants.BROADCAST_BUFFERING);
        registerReceiver(nextTrackReceiver, new IntentFilter(Constants.BROADCAST_NEXT_TRACK));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.release();
            player = null;
        }
        if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
        cancelNotification();
        Constants.playIndex = -1;
        unregisterReceiver(nextTrackReceiver);
        sendBroadcastButtonStatus();

        unregisterReceiver(seekBarReceiver);
        handler.removeCallbacks(sendUpdatesToUI);

    }

    private void updateSeekPos(Intent intent) {
        int seekPos = intent.getIntExtra("seekpos", 0);
        if (player.isPlaying()) {
            handler.removeCallbacks(sendUpdatesToUI);
            player.seekTo(seekPos);
            sendBufferingBroadcast();
            setupHandler();
        }
    }

    private void setupHandler() {
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 200);
    }

    private void logMediaPosition() {
        if (player.isPlaying()) {
            int mediaPos = player.getCurrentPosition();
            int mediaMax = player.getDuration();
            SeekBarPosDispatcher.notifyAllUpdatePosition(mediaPos, mediaMax);
        }
    }

    private void sendBufferingBroadcast() {
        bufferIntent.putExtra("buffering", "1");
        bufferIntent.putExtra(Constants.INDEX, index);
        bufferIntent.putExtra(Constants.HIKE_ID, hikeId);
        sendBroadcast(bufferIntent);
    }

    private void sendBufferedCompleteBroadcast() {
        bufferIntent.putExtra("buffering", "0");
        bufferIntent.putExtra(Constants.INDEX, index);
        bufferIntent.putExtra(Constants.HIKE_ID, hikeId);
        sendBroadcast(bufferIntent);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
    }

    @Override
    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    @Override
    public void onSeekComplete(MediaPlayer mediaPlayer) {
        sendBufferedCompleteBroadcast();
    }
}
