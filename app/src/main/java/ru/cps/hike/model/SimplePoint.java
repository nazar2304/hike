package ru.cps.hike.model;

public class SimplePoint {
    private double latitudeY;
    private double longitudeX;

    public SimplePoint(double latitudeY, double longitudeX) {
        this.latitudeY = latitudeY;
        this.longitudeX = longitudeX;
    }

    public double getLatitudeY() {
        return latitudeY;
    }

    public void setLatitudeY(double latitudeY) {
        this.latitudeY = latitudeY;
    }

    public double getLongitudeX() {
        return longitudeX;
    }

    public void setLongitudeX(double longitudeX) {
        this.longitudeX = longitudeX;
    }
}
