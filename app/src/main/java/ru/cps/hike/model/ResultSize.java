package ru.cps.hike.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultSize {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private Size data;

    public Integer getSize() {
        if(data == null){
            return 0;
        }
        return data.getTotalSizeMb();
    }
}
