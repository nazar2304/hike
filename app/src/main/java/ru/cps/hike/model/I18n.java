package ru.cps.hike.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class I18n {

    @SerializedName("en")
    @Expose
    private Lang en;
    @SerializedName("ru")
    @Expose
    private Lang ru;

    public Lang getEn() {
        return en;
    }

    public void setEn(Lang en) {
        this.en = en;
    }

    public Lang getRu() {
        return ru;
    }

    public void setRu(Lang ru) {
        this.ru = ru;
    }

}