package ru.cps.hike.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import ru.cps.hike.App;
import ru.cps.hike.Constants;

public class Hike {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image")
    @Expose
    private Image image;
    @SerializedName("authorId")
    @Expose
    private String authorId;
    @SerializedName("cityId")
    @Expose
    private String cityId;
    @SerializedName("countryId")
    @Expose
    private String countryId;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("duration")
    @Expose
    private Long duration;
    @SerializedName("pointsCount")
    @Expose
    private Integer pointsCount;
    @SerializedName("i18n")
    @Expose
    private I18n i18n;
    @SerializedName("points")
    @Expose
    private List<Point> points;
    private Integer size;

    public Integer getSize() {
        if (size == null) {
            return 0;
        }
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getPointsCount() {
        if(pointsCount == null){
            return points.size();
        }
        return pointsCount;
    }

    public void setPointsCount(Integer pointsCount) {
        this.pointsCount = pointsCount;
    }

    public Lang getLang() {
        if(App.getCurrentLang().equals("ru")){
            if(i18n.getRu() == null){
                return i18n.getEn();
            }
            return  i18n.getRu();
        } else {
            if(i18n.getEn() == null){
                return i18n.getRu();
            }
            return  i18n.getEn();
        }
    }

    public void setI18n(I18n i18n) {
        this.i18n = i18n;
    }

    public List<Point> getPoints() {
        if(points == null){
            points = new ArrayList<>();
        }
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
}
