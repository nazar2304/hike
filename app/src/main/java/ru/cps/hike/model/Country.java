package ru.cps.hike.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.cps.hike.App;
import ru.cps.hike.Constants;

public class Country {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image")
    @Expose
    private Image image;
    @SerializedName("zoom")
    @Expose
    private Integer zoom;
    @SerializedName("x")
    @Expose
    private Double x;
    @SerializedName("y")
    @Expose
    private Double y;
    @SerializedName("hikesCount")
    @Expose
    private Integer hikesCount;
    @SerializedName("citiesCount")
    @Expose
    private Integer citiesCount;
    @SerializedName("i18n")
    @Expose
    private I18n i18n;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Integer getZoom() {
        return zoom;
    }

    public void setZoom(Integer zoom) {
        this.zoom = zoom;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Integer getHikesCount() {
        return hikesCount;
    }

    public void setHikesCount(Integer hikesCount) {
        this.hikesCount = hikesCount;
    }

    public Integer getCitiesCount() {
        return citiesCount;
    }

    public void setCitiesCount(Integer citiesCount) {
        this.citiesCount = citiesCount;
    }

    public Lang getLang() {
        if(App.getCurrentLang().equals("ru")){
            if(i18n.getRu() == null){
                return i18n.getEn();
            }
            return  i18n.getRu();
        } else {
            if(i18n.getEn() == null){
                return i18n.getRu();
            }
            return  i18n.getEn();
        }
    }

    public void setI18n(I18n i18n) {
        this.i18n = i18n;
    }

}