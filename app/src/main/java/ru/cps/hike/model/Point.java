package ru.cps.hike.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.cps.hike.App;
import ru.cps.hike.Constants;

public class Point {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("images")
    @Expose
    private ArrayList<Image> images = null;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("hikeId")
    @Expose
    private String hikeId;
    @SerializedName("x")
    @Expose
    private Double x;
    @SerializedName("y")
    @Expose
    private Double y;
    @SerializedName("reviewVersionFlag")
    @Expose
    private Boolean reviewVersionFlag;
    @SerializedName("i18n")
    @Expose
    private I18n i18n;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getHikeId() {
        return hikeId;
    }

    public void setHikeId(String hikeId) {
        this.hikeId = hikeId;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Boolean getReviewVersionFlag() {
        return reviewVersionFlag;
    }

    public void setReviewVersionFlag(Boolean reviewVersionFlag) {
        this.reviewVersionFlag = reviewVersionFlag;
    }

    public Lang getLang() {
        if (i18n == null) {
            i18n = new I18n();
        }
        if (App.getCurrentLang().equals("ru")) {
            if (i18n.getRu() == null && i18n.getEn() == null) {
                i18n.setRu(new Lang());
            } else if (i18n.getRu() == null) {
                return i18n.getEn();
            }
            return i18n.getRu();
        } else {
            if (i18n.getEn() == null && i18n.getRu() == null) {
                i18n.setEn(new Lang());
            } else if (i18n.getEn() == null) {
                return i18n.getRu();
            }
            return i18n.getEn();
        }
    }

    public void setI18n(I18n i18n) {
        this.i18n = i18n;
    }

}
