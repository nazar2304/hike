package ru.cps.hike.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Size {

    @SerializedName("totalSize")
    @Expose
    private Integer totalSize;

    Integer getTotalSizeMb() {
        return totalSize / 1048576;
    }
}