package ru.cps.hike.connection;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import ru.cps.hike.model.ResultCities;
import ru.cps.hike.model.ResultCountries;
import ru.cps.hike.model.ResultHike;
import ru.cps.hike.model.ResultPoints;
import ru.cps.hike.model.ResultSize;

import static ru.cps.hike.connection.ApiConstant.*;

public interface Api {

    @GET(POINTS)
    Observable<ResultPoints> getPoints(@QueryMap Map<String, String> fields);

    @GET(GET_COUNTRIES)
    Observable<ResultCountries> getCountries(@QueryMap Map<String, String> fields);

    @GET(GET_CITIES)
    Observable<ResultCities> getCities(@QueryMap Map<String, String> fields);


    @GET(GET_HIKES)
    Observable<ResultHike> getHikes(@QueryMap Map<String, String> fields);

    @GET("/api/v2/hikes/{id}/contentSize")
    Observable<ResultSize> getHikeSize(@Path(value = "id", encoded = true) String id, @QueryMap Map<String, String> fields);
}
