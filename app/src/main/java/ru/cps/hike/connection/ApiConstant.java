package ru.cps.hike.connection;

public class ApiConstant {
    static final String POINTS = "/api/v2/points/";
    static final String GET_COUNTRIES = "/api/v2/countries";
    static final String GET_CITIES = "/api/v2/cities";
    static final String GET_HIKES = "/api/v2/hikes";
    static final String GET_POINTS = "/api/v2/points/";

}
