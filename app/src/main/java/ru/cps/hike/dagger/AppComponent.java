package ru.cps.hike.dagger;


import javax.inject.Singleton;

import dagger.Component;
import ru.cps.hike.dagger.module.ApiModule;
import ru.cps.hike.dagger.module.ContextModule;
import ru.cps.hike.dagger.module.LocalStorageModule;
import ru.cps.hike.screens.catalog.cities.CitiesPresenter;
import ru.cps.hike.screens.catalog.country.CountriesPresenter;
import ru.cps.hike.screens.catalog.hikes.HikesPresenter;
import ru.cps.hike.screens.my_hikes.MyHikesPresenter;
import ru.cps.hike.screens.point.PointExpListPresenter;


@Singleton
@Component(modules = {ContextModule.class, ApiModule.class, LocalStorageModule.class})
public interface AppComponent {

    void inject(PointExpListPresenter example);

    void inject(CountriesPresenter example);

    void inject(CitiesPresenter example);

    void inject(HikesPresenter example);

    void inject(MyHikesPresenter example);
}

