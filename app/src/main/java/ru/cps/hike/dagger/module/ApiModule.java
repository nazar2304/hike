package ru.cps.hike.dagger.module;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import ru.cps.hike.connection.Api;

@Module(includes = {RetrofitModule.class})
public class ApiModule {

    @Provides
    @Singleton
    Api provideApi(Retrofit retrofit) {
        return retrofit.create(Api.class);
    }
}
