package ru.cps.hike.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import ru.cps.hike.R;
import ru.cps.hike.controller.http.DownloadHikeListener;
import ru.cps.hike.screens.map.MapFragment;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;

public class ButtonCangeStyle {

    public static void setButtonDownloadStart(Button btn, Hike h, Context cont) {
        try {
            btn.setText(btn.getContext().getString(R.string.dowload));
            btn.setBackground(btn.getContext().getResources().getDrawable(R.drawable.btn_bg_download));
            btn.setEnabled(true);
            btn.setOnClickListener(new DownloadHikeListener(h.getId(), h.getLang().getTitle(), h.getImage().getPreview(), h.getCityId(), (Activity) cont, h.getPointsCount()));
        } catch (Exception e) {
            Log.e("TAG", Log.getStackTraceString(e));
        }
    }

    public static void setButtonDownloading(Button btn) {
        btn.setText(btn.getContext().getString(R.string.downloading));
        btn.setBackground(btn.getContext().getResources().getDrawable(R.drawable.btn_bg_no_connection));
        btn.setEnabled(false);
    }

    public static void setButtonDownloadComplete(Button btn, final String hikeI, final int mapStart, final int hikeSource) {
        btn.setText(btn.getContext().getString(R.string.to_map));
        btn.setBackground(btn.getContext().getResources().getDrawable(R.drawable.btn_bg_to_map));
        btn.setEnabled(true);
        btn.setOnClickListener(view -> {
            Intent intent = new Intent(MapFragment.BROADCAST_ACTION_HIKE_ID);
            intent.putExtra(MapFragment.HIKE_ID, hikeI);
            intent.putExtra(MapFragment.POINT_ID, 0);
            intent.putExtra(Constants.PLAY, false);
            intent.putExtra(Constants.MAP_START, mapStart);
            intent.putExtra(Constants.HIKE_SOURCE, hikeSource);

            btn.getContext().sendBroadcast(intent);
        });
    }

    public static void setButtonDownloadNoConnection(Button btn) {
        btn.setText(btn.getContext().getString(R.string.no_connection));
        btn.setBackground(btn.getContext().getResources().getDrawable(R.drawable.btn_bg_no_connection));
        btn.setEnabled(true);
        btn.setOnClickListener(view -> Toast.makeText(btn.getContext(), btn.getContext().getString(R.string.check_connection), Toast.LENGTH_SHORT).show());
    }
}
