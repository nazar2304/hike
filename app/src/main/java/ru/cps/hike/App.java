package ru.cps.hike;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.StrictMode;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.cps.hike.dagger.AppComponent;
import ru.cps.hike.dagger.DaggerAppComponent;
import ru.cps.hike.dagger.module.ContextModule;

import static ru.cps.hike.Constants.HIKE_CHANEL_ID;

public class App extends Application {
    private static AppComponent sAppComponent;
    private static App app;
    private float dp;
    public static List<String> downloadList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
        app = this;
        dp = getResources().getDisplayMetrics().density;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O && notificationManager != null) {
            NotificationChannel mChannel = new NotificationChannel(HIKE_CHANEL_ID, HIKE_CHANEL_ID, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription("Hike");
            mChannel.enableLights(false);
            mChannel.enableVibration(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static String getCurrentLang() {
        if (Locale.getDefault().getLanguage().equals("ru")) {
            return "ru";
        } else {
            return "en";
        }
    }


    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static App getApp() {
        return app;
    }

    public float getDp() {
        return dp;
    }
}
