package ru.cps.hike.screens.catalog.cities;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.cps.hike.App;
import ru.cps.hike.base.BasePresenter;
import ru.cps.hike.connection.Api;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.dagger.module.LocalStorage;
import ru.cps.hike.model.City;
import ru.cps.hike.Constants;
import ru.cps.hike.model.ResultCities;

@InjectViewState
public class CitiesPresenter extends BasePresenter<CitiesView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;

    private List<City> cityList = new ArrayList<>();
    private String country;
    private String countryId;

    CitiesPresenter() {
        App.getAppComponent().inject(this);
    }

    void setCountry(String country, String countryId, int citiesNumber) {
        this.country = country;
        this.countryId = countryId;

        getViewState().setCitiesTitle(country, citiesNumber);
    }

    public String getCountry() {
        return country;
    }

    List<City> getCityList() {
        return cityList;
    }

    void load(){
        Map<String, String> query = new HashMap<>();
        query.put("country", countryId);
        query.put("lang", App.getCurrentLang());

        getViewState().showProgress(true);

        Disposable disposable = mApi.getCities(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            cityList.clear();
                            cityList.addAll(result.getData());
                            getViewState().notifyList();
                            FileTools.saveRespond(new Gson().toJson(result), countryId);
                            getViewState().showProgress(false);
                            getViewState().showEmpty(false);
                        }
                        , throwable -> {
                            String countiesString = FileTools.readRespond(countryId);
                            if (!TextUtils.isEmpty(countiesString)) {
                                ResultCities result = new Gson().fromJson(countiesString, ResultCities.class);
                                cityList.clear();
                                cityList.addAll(result.getData());
                                getViewState().notifyList();
                                getViewState().showEmpty(false);
                            } else {
                                getViewState().showEmpty(true);
                            }
                            getViewState().showProgress(false);
                        });
        unsubscribeOnDestroy(disposable);
    }
}
