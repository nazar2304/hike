package ru.cps.hike.screens.catalog.country;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.base.BaseFragment;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Country;
import ru.cps.hike.screens.catalog.cities.CitiesFragment;
import ru.cps.hike.screens.catalog.BaseContainerFragment;


public class CountriesFragment extends BaseFragment implements CountriesView {

    @BindView(R.id.listView) ListView mListView;
    @BindView(R.id.empty) View mEmpty;
    @InjectPresenter CountriesPresenter mPresenter;

    private CountryAdapter adapter;
    private boolean toUpdateList = true;
    private String tabTag;

    private BroadcastReceiver onLineReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null) {
                    boolean isConn = netInfo.isConnectedOrConnecting();
                    if (toUpdateList && isConn) {
                        mPresenter.load();
                        toUpdateList = false;
                    }
                }
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_countries, container, false);
        unbinder = ButterKnife.bind(this, view);
        progress = view.findViewById(R.id.progress);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListView.setOnItemClickListener((parent, view1, position, id) -> {
            Country c = adapter.getCountry(position);
            if (c.getCitiesCount() > 0) {
                CitiesFragment citiesFragment = new CitiesFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.COUNTRY, c.getLang().getName());
                bundle.putString(Constants.COUNTRY_ID, c.getId());
                bundle.putInt(Constants.CITY, c.getCitiesCount());
                citiesFragment.setTabTag(tabTag);
                citiesFragment.setArguments(bundle);
                if (getParentFragment() != null)
                    ((BaseContainerFragment) getParentFragment()).replaceFragment(citiesFragment, true);
            } else {
                Toast.makeText(App.getApp(), R.string.no_cyties, Toast.LENGTH_SHORT).show();
            }
        });
        adapter = new CountryAdapter(getContext(), mPresenter.getCountries());
        mListView.setAdapter(adapter);
        mListView.setDivider(null);

        if (getActivity() != null && !getActivity().getPackageName().equals("ru.cps.countryonhand") && mPresenter.getCountries().size() == 0) {
            mPresenter.load();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter onLineFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (getActivity() != null)
            getActivity().registerReceiver(onLineReceiver, onLineFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null)
            getActivity().unregisterReceiver(onLineReceiver);
    }

    @Override
    public void notifyList() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showEmpty(boolean status) {
        mEmpty.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    public void setTabTag(String tabTag) {
        this.tabTag = tabTag;
    }
}
