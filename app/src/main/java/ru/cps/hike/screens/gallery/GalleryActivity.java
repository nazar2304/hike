package ru.cps.hike.screens.gallery;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.ArrayList;

import ru.cps.hike.R;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Image;


public class GalleryActivity extends FragmentActivity {
    private ArrayList<Image> mImages;
    private String hikeId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actyvity_galary);

        ViewPager viewPager = findViewById(R.id.vp_galary);

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        mImages = getIntent().getParcelableArrayListExtra(Constants.IMAGES);
        hikeId = getIntent().getStringExtra(Constants.ID);

        FragmentManager fm = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public int getCount() {
                return mImages.size();
            }

            @Override
            public Fragment getItem(int pos) {
                return GalleryElementFragment.newInstance(mImages.get(pos), hikeId);
            }
        });

        viewPager.setClipToPadding(false);
        viewPager.setPadding(30, 0, 30, 0);
        viewPager.setPageMargin(10);
    }
}
