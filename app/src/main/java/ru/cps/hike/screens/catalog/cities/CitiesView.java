package ru.cps.hike.screens.catalog.cities;

import ru.cps.hike.base.BaseView;

public interface CitiesView extends BaseView {
    void showEmpty(boolean status);

    void setCitiesTitle(String country, int count);

    void notifyList();
}
