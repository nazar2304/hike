package ru.cps.hike.screens.catalog.hikes;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.cps.hike.R;
import ru.cps.hike.base.BaseFragment;
import ru.cps.hike.controller.dowload_hike_listener.DownloadHikeDispatcher;
import ru.cps.hike.controller.dowload_hike_listener.IDownloadStatusListener;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;
import ru.cps.hike.screens.point.PointExpListActivity;


public class HikesFragment extends BaseFragment implements IDownloadStatusListener, HikesView {

    HikeAdapter adapter;
    String tabTag;
    private boolean firstLaunch = true;


    BroadcastReceiver onLineReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null) {
                    boolean isConn = netInfo.isConnectedOrConnecting();
                    if (isConn) {
                        adapter.notifyDataSetChanged();
                        if (toUpdateList) {
                            mPresenter.load();
                            toUpdateList = false;
                        }
                    } else {
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    adapter.notifyDataSetChanged();
                }
            }
        }
    };
    boolean toUpdateList = true;

    @InjectPresenter HikesPresenter mPresenter;
    @BindView(R.id.listView) ListView mListView;
    @BindView(R.id.empty) View mEmpty;
    @BindView(R.id.hikesTitle) TextView mHikesTitle;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hikes, container, false);
        unbinder = ButterKnife.bind(this, view);
        progress = view.findViewById(R.id.progress);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListView.setOnItemClickListener((parent, view1, position, id) -> {
            Hike h = adapter.getHike(position);
            Intent intent = new Intent(getContext(), PointExpListActivity.class);
            intent.putExtra(Constants.ID, h.getId());
            intent.putExtra(Constants.HIKE_SOURCE, Constants.HIKE_FROM_CATALOG);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (tabTag.equals(getResources().getString(R.string.title_catalog))) {
                intent.putExtra(Constants.VISIBILITY, true);
            }
            startActivity(intent);
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            mPresenter.setCity(bundle.getString(Constants.COUNTRY, " "), bundle.getString(Constants.CITY, " "), bundle.getString(Constants.CITY_ID, "no argument pass"));
            adapter = new HikeAdapter(getContext(), mPresenter.getHikes(), bundle.getString(Constants.CITY_ID, "no argument pass"));
            mListView.setAdapter(adapter);
        }

        if (firstLaunch) {
            mPresenter.load();
            firstLaunch = false;
        }
        DownloadHikeDispatcher.addListener(this);
    }

    @Override
    public void onDownloadComplete(int resCode, String hikeId) {
        adapter.notifyDataSetChanged();
    }


    public void setTabTag(String tabTag) {
        this.tabTag = tabTag;
    }


    @Override
    public void onResume() {
        super.onResume();
        IntentFilter onLineFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (getActivity() != null)
            getActivity().registerReceiver(onLineReceiver, onLineFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null)
            getActivity().unregisterReceiver(onLineReceiver);
        ImageLoader.getInstance().clearMemoryCache();
    }

    @Override
    public void showEmpty(boolean status) {
        mEmpty.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setHikesTitle(String country, String city) {
        mHikesTitle.setText(String.format(getResources().getString(R.string.placeholder_hikes_title), country, city));

    }

    @Override
    public void notifyList() {
        adapter.notifyDataSetChanged();
    }
}


