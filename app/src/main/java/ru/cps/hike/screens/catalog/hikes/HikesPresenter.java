package ru.cps.hike.screens.catalog.hikes;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.cps.hike.App;
import ru.cps.hike.base.BasePresenter;
import ru.cps.hike.connection.Api;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.dagger.module.LocalStorage;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;
import ru.cps.hike.model.ResultHike;

@InjectViewState
public class HikesPresenter extends BasePresenter<HikesView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;

    private List<Hike> hikes = new ArrayList<>();

    private String cityId;

    void setCity(String country, String city, String cityId) {
        this.cityId = cityId;
        getViewState().setHikesTitle(country, city);
    }

    public List<Hike> getHikes() {
        return hikes;
    }

    HikesPresenter() {
        App.getAppComponent().inject(this);
    }

    void load() {
        Map<String, String> query = new HashMap<>();
        query.put("city", cityId);
        query.put("lang", App.getCurrentLang());

        /*try {
            String res = FileTools.readRespond(cityId);
            if (res != null) {
                ResultHike result = new Gson().fromJson(res, ResultHike.class);
                hikes.clear();
                hikes.addAll(result.getData());
                getViewState().notifyList();
                return;
            }
        } catch (Exception ignored) {
        }*/
        getViewState().showProgress(true);
        Disposable disposable = mApi.getHikes(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            hikes.clear();
                            hikes.addAll(result.getData());

                            for (int i = 0; i < hikes.size(); i++) {
                                int finalI = i;
                                Disposable disposableSize = mApi.getHikeSize(hikes.get(i).getId(), query)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(resultSize -> {
                                                    hikes.get(finalI).setSize(resultSize.getSize());
                                                    result.getData().get(finalI).setSize(resultSize.getSize());
                                                    FileTools.saveRespond(new Gson().toJson(result), cityId);
                                                    FileTools.saveExport(new Gson().toJson(hikes.get(finalI)), hikes.get(finalI).getId());
                                                    getViewState().notifyList();
                                                }
                                                , throwable -> {
                                                });
                                unsubscribeOnDestroy(disposableSize);
                            }
                            getViewState().notifyList();
                            FileTools.saveRespond(new Gson().toJson(result), cityId);
                            getViewState().showProgress(false);
                            getViewState().showEmpty(false);
                        }
                        , throwable -> {
                            String res = FileTools.readRespond(cityId);
                            if (!TextUtils.isEmpty(res)) {
                                ResultHike result = new Gson().fromJson(res, ResultHike.class);
                                hikes.clear();
                                hikes.addAll(result.getData());
                                getViewState().notifyList();
                                getViewState().showEmpty(false);
                            } else {
                                getViewState().showEmpty(true);
                            }
                            getViewState().showProgress(false);
                        });
        unsubscribeOnDestroy(disposable);
    }
}
