package ru.cps.hike.screens.point;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.controller.audio.AudioService;
import ru.cps.hike.controller.audio.ISeekBarPosListener;
import ru.cps.hike.controller.audio.SeekBarPosDispatcher;
import ru.cps.hike.controller.audio.SimplePlayListener;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.controller.http.StringRequests;
import ru.cps.hike.model.Hike;
import ru.cps.hike.screens.map.MapFragment;
import ru.cps.hike.Constants;
import ru.cps.hike.screens.gallery.GalleryActivity;

public class NewPointExpListAdapter extends BaseExpandableListAdapter implements SeekBar.OnSeekBarChangeListener, ISeekBarPosListener {

    private boolean isHikeDownloaded;
    private Context mContext;
    private boolean toPlay;
    private DisplayImageOptions options;
    private SeekBar seekBar;
    private Hike hike;

    NewPointExpListAdapter(Context context, Hike hike, boolean toPlay) {
        this.hike = hike;
        mContext = context;
        this.toPlay = toPlay;
        this.isHikeDownloaded = SaveInSharedPref.isHikeDownloaded(hike.getId());

        options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.no_image)
                .showImageOnFail(R.drawable.no_image)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        SeekBarPosDispatcher.addListener(this);
    }


    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (groupPosition == 0) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater != null)
                convertView = inflater.inflate(R.layout.group_view0, parent, false);

            ImageView preview = convertView.findViewById(R.id.iv_point_preview);

            if (SaveInSharedPref.isHikeDownloaded(hike.getId())) {
                String sb = "file:///" + FileTools.getFolder() + "/" + hike.getId() + "/" + new File(hike.getImage().getPreview()).getName();
                ImageLoader.getInstance().displayImage(sb, preview, options);
            } else {
                ImageLoader.getInstance().displayImage(App.getApp().getResources().getString(R.string.general) + hike.getImage().getPreview(), preview, options);
            }

            TextView title = convertView.findViewById(R.id.tv_point_zero_title);
            TextView description = convertView.findViewById(R.id.tv_point_decription);


            StringBuilder hikeMetaData = new StringBuilder();
            hikeMetaData.append(mContext.getString(R.string.points_amount));
            hikeMetaData.append("  ");


            if (hike.getPointsCount() != 0) {
                hikeMetaData.append(hike.getPointsCount());
            } else {
                if (hike.getPoints() != null) {
                    hikeMetaData.append(hike.getPoints().size());
                } else {
                    hikeMetaData.append("0");
                }
            }

            title.setText(hikeMetaData.toString() + "\n" + title.getResources().getString(R.string.size) + "  " + hike.getSize() + " " + title.getResources().getString(R.string.mb));

            description.setText(Html.fromHtml(hike.getLang().getDescription()));
            description.setMovementMethod(LinkMovementMethod.getInstance());
            return convertView;
        }
        if (!StringRequests.isOnline()) {
            if (hike.getPoints().get(groupPosition - 1).getLang().getDescription().equals(mContext.getString(R.string.for_view_need_internet))) {

                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (inflater != null)
                    convertView = inflater.inflate(R.layout.group_view0, parent, false);
                ImageView preview = convertView.findViewById(R.id.iv_point_preview);
                preview.setVisibility(View.GONE);
                TextView title = convertView.findViewById(R.id.tv_point_zero_title);
                title.setText(mContext.getString(R.string.for_view_need_internet));

                convertView.setOnClickListener(v -> {
                });
                convertView.setOnTouchListener((v, event) -> false);
                return convertView;
            }
        }


        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflater != null) {
            view = inflater.inflate(R.layout.group_view, parent, false);
            ImageView preview = view.findViewById(R.id.iv_point_preview);
            ImageView loupe = view.findViewById(R.id.iv_point_loupe);
            TextView title = view.findViewById(R.id.tv_point_title);
            if (isExpanded) {
                preview.setOnClickListener(view1 -> {
                    Intent intent = new Intent(mContext, GalleryActivity.class);
                    intent.putParcelableArrayListExtra(Constants.IMAGES, hike.getPoints().get(groupPosition).getImages());
                    intent.putExtra(Constants.ID, hike.getId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                });
                loupe.setVisibility(View.VISIBLE);
            }

            int position = groupPosition - 1;
            if (position < hike.getPoints().size()) {
                String imgName = hike.getPoints().get(position).getImages().get(0).getPreview();
                title.setText(String.format(title.getResources().getString(R.string.placeholder_index), String.valueOf(groupPosition), hike.getPoints().get(position).getLang().getTitle()));
                if (SaveInSharedPref.isHikeDownloaded(hike.getId())) {
                    String sb = "file:///" + FileTools.getFolder() + "/" + hike.getId() + "/" + new File(imgName).getName();
                    ImageLoader.getInstance().displayImage(sb, preview);
                } else {
                    ImageLoader.getInstance().displayImage(App.getApp().getResources().getString(R.string.general) + imgName, preview, options);
                }

                if (!isHikeDownloaded) {
                    if (!hike.getPoints().get(position).getReviewVersionFlag()) {
                        view.setEnabled(false);
                        view.setClickable(false);
                    }
                }
            }
        }

        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.isHikeDownloaded = SaveInSharedPref.isHikeDownloaded(hike.getId());
    }


    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = convertView;
        if (groupPosition != 0) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater != null)
                if (view == null) {
                    view = inflater.inflate(R.layout.child_view, parent, false);
                }
            seekBar = view.findViewById(R.id.sb_play_progress);
            seekBar.setOnSeekBarChangeListener(this);

            if (groupPosition - 1 == Constants.playIndex && hike.getId().equals(Constants.playingHikeId)) {
                showSeek();
            } else {
                hideSeek();
            }

            TextView description = view.findViewById(R.id.tv_descr);
            final Button btnPlay = view.findViewById(R.id.btn_play);
            final Button btnToMap = view.findViewById(R.id.btn_to_map);

            description.setText(Html.fromHtml(hike.getPoints().get(groupPosition - 1).getLang().getDescription()));
            description.setMovementMethod(LinkMovementMethod.getInstance());


            btnPlay.setOnClickListener(SimplePlayListener.getInstance(hike.getId(), hike.getPoints().get(groupPosition - 1).getLang().getAudio(), hike.getPoints().get(groupPosition - 1).getLang().getTitle(), hike.getPoints().get(groupPosition - 1).getImages().get(0).getPreview(), groupPosition - 1, isHikeDownloaded));

            if (Constants.playingHikeId.equals(hike.getId())) {
                if (Constants.playIndex == groupPosition - 1) {
                    if (!Constants.playerPaused) {
                        btnPlay.setBackgroundResource(R.drawable.pause);
                    } else {
                        btnPlay.setBackgroundResource(R.drawable.play);
                    }
                } else {
                    btnPlay.setBackgroundResource(R.drawable.play);
                }
            }

            btnToMap.setBackground(mContext.getResources().getDrawable(R.drawable.btn_bg_to_map));
            btnToMap.setEnabled(true);

            btnToMap.setOnClickListener(view1 -> {
                Intent intent = new Intent(MapFragment.BROADCAST_ACTION_HIKE_ID);
                intent.putExtra(MapFragment.HIKE_ID, hike.getId());
                intent.putExtra(MapFragment.POINT_POSITION, groupPosition - 1);
                intent.putExtra(Constants.PLAY, toPlay);
                intent.putExtra(Constants.MAP_START, 0);
                App.getApp().sendBroadcast(intent);
                ((PointExpListActivity) mContext).finish();
            });
        }
        return view;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (b) {
            int seekPos = seekBar.getProgress();
            Intent seekBarIntent = new Intent(Constants.BROADCAST_SEEKBAR);
            seekBarIntent.putExtra("seekpos", seekPos);
            mContext.sendBroadcast(seekBarIntent);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void updatePosition(int seekProgress, int seekMax) {
        if (seekBar != null) {
            seekBar.setProgress(seekProgress);
            seekBar.setMax(seekMax);
        }
    }

    @Override
    public void hideSeek() {
        if (seekBar != null) {
            seekBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showSeek() {
        if (seekBar != null) {
            seekBar.setMax(AudioService.getMaxPosition());
            seekBar.setProgress(AudioService.getCurrentPosition());
            seekBar.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getGroupCount() {
        return hike.getPoints().size() + 1;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return hike.getPoints().get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);

    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);

    }
}