package ru.cps.hike.screens.my_hikes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;
import ru.cps.hike.views.ButtonCangeStyle;


public class HikeAdapter extends BaseAdapter {
    private LayoutInflater lInflater;
    private List<Hike> hikesList;
    private DisplayImageOptions options;

    HikeAdapter(Context context, List<Hike> hikes) {
        hikesList = hikes;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.no_image)
                .showImageOnFail(R.drawable.no_image)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        final Hike h = getHike(position);
        ViewHolder holder;
        if (view == null) {
            view = lInflater.inflate(R.layout.hike_item_dowloaded, parent, false);

            holder = new ViewHolder();

            holder.imageView = view.findViewById(R.id.iv_my_hike_image);
            holder.tvHikeName = view.findViewById(R.id.tv_my_hike_name);
            holder.btnDownload = view.findViewById(R.id.btn_to_map_hike);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String sb = h.getLang().getTitle() + "\n" + "\n" + holder.imageView.getResources().getString(R.string.points) + " " + h.getPointsCount() + "\n" + holder.imageView.getResources().getString(R.string.size) + "  " + h.getSize() + " " + holder.imageView.getResources().getString(R.string.mb);
        holder.tvHikeName.setText(sb);
        ButtonCangeStyle.setButtonDownloadComplete(holder.btnDownload, h.getId(), Constants.MAP_START_FROM_TAB, Constants.HIKE_DOWNLOADED);


        String imgName = h.getImage().getPreview();
        if (SaveInSharedPref.isHikeDownloaded(h.getId())) {
            String imgFile = "file:///" + FileTools.getFolder() + "/" + h.getId() + "/" + imgName;
            ImageLoader.getInstance().displayImage(imgFile, holder.imageView);
        } else {
            ImageLoader.getInstance().displayImage(App.getApp().getResources().getString(R.string.general) + imgName, holder.imageView, options);
        }
        return view;
    }

    Hike getHike(int position) {
        return ((Hike) getItem(position));
    }

    @Override
    public int getCount() {
        return hikesList.size();
    }

    @Override
    public Object getItem(int position) {
        return hikesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        ImageView imageView;
        TextView tvHikeName;
        Button btnDownload;
    }
}

