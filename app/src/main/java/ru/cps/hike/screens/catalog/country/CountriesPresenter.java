package ru.cps.hike.screens.catalog.country;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.cps.hike.App;
import ru.cps.hike.base.BasePresenter;
import ru.cps.hike.connection.Api;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.dagger.module.LocalStorage;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Country;
import ru.cps.hike.model.ResultCountries;

@InjectViewState
public class CountriesPresenter extends BasePresenter<CountriesView> {

    private List<Country> countryList = new ArrayList<>();

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;

    CountriesPresenter() {
        App.getAppComponent().inject(this);
    }

    void load() {
        Map<String, String> query = new HashMap<>();
        query.put("lang", App.getCurrentLang());

        getViewState().showProgress(true);


        Disposable disposable = mApi.getCountries(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            countryList.clear();
                            countryList.addAll(result.getData());
                            getViewState().notifyList();
                            getViewState().showProgress(false);
                            getViewState().showEmpty(false);
                            FileTools.saveRespond(new Gson().toJson(result), Constants.COUNTRIES_FILE_NAME);
                        }
                        , throwable -> {
                            String countiesString = FileTools.readRespond(Constants.COUNTRIES_FILE_NAME);
                            if (!TextUtils.isEmpty(countiesString)) {
                                ResultCountries result = new Gson().fromJson(countiesString, ResultCountries.class);
                                if (result != null){
                                    countryList.clear();
                                    countryList.addAll(result.getData());
                                    getViewState().showEmpty(false);
                                    getViewState().notifyList();
                                }
                            } else {
                                getViewState().showEmpty(true);
                            }
                            getViewState().showProgress(false);
                        });
        unsubscribeOnDestroy(disposable);
    }

    List<Country> getCountries() {
        return countryList;
    }
}
