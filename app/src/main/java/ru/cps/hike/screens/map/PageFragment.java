package ru.cps.hike.screens.map;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.controller.audio.AudioService;
import ru.cps.hike.controller.audio.ISeekBarPosListener;
import ru.cps.hike.controller.audio.SeekBarPosDispatcher;
import ru.cps.hike.controller.audio.SimplePlayListener;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.controller.http.StringRequests;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Point;
import ru.cps.hike.screens.point.PointExpListActivity;


public class PageFragment extends Fragment implements SeekBar.OnSeekBarChangeListener, ISeekBarPosListener {
    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    static final String ARGUMENT_IMAGE_PATH = "arg_image_path";
    static final String ARGUMENT_TITLE = "arg_title";
    static final String ARGUMENT_HIKE_ID = "arg_hike_id";
    static final String ARGUMENT_TRACK_ID = "arg_track_id";
    static final String ARGUMENT_PLAY = "play";
    static final String ARGUMENT_reviewVersionFlag = "reviewVersionFlag";
    static final String ARGUMENT_IS_HIKE_DOWNLOADED = "isHikeDownloaded";
    static final String ARGUMENT_HIKE_TITLE = "ARGUMENT_HIKE_TITLE";
    static final String ARGUMENT_CITY_ID = "ARGUMENT_CITY_ID";
    static final String ARGUMENT_HIKE_PREVIEW = "ARGUMENT_HIKE_PREVIEW";
    private static DisplayImageOptions options;
    boolean isHikeDownloaded;
    int pageNumber;
    Button btnPage;
    String hikeTitle;
    String hikePreview;
    String cityId;
    BroadcastReceiver stopPlayerReceiver;
    BroadcastReceiver downloadCompleteReceiver;
    Animation animation;
    Intent seekBarIntent;
    private String image;
    private String title;
    private String hikeId;
    private String trackId;
    private boolean reviewVersionFlag;
    private boolean toPlay;
    private SeekBar seekBar;
    private ProgressDialog pdBuff = null;
    private BroadcastReceiver broadcastBufferReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int gotIndex = intent.getIntExtra(Constants.INDEX, -1);
            String gotHikeId = intent.getStringExtra(Constants.HIKE_ID);
            if (gotHikeId.equals(hikeId)) {
                if (mUserVisibleHint && gotIndex == pageNumber) {
                    showPD(intent);
                }
            }
        }
    };
    boolean mUserVisibleHint = false;

    public static PageFragment newInstance(int page, Point point, boolean toPlay, boolean isHikeDownloaded, String hikeTitle, String hikePreview, String cityId, String hikeId) {

        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        arguments.putString(ARGUMENT_IMAGE_PATH, point.getImages().get(0).getPreview());
        arguments.putString(ARGUMENT_TITLE, point.getLang().getTitle());
        arguments.putString(ARGUMENT_HIKE_ID, hikeId);
        arguments.putString(ARGUMENT_TRACK_ID, point.getLang().getAudio());
        arguments.putBoolean(ARGUMENT_PLAY, toPlay);
        arguments.putBoolean(ARGUMENT_reviewVersionFlag, point.getReviewVersionFlag());
        arguments.putBoolean(ARGUMENT_IS_HIKE_DOWNLOADED, isHikeDownloaded);

        arguments.putString(ARGUMENT_HIKE_TITLE, hikeTitle);
        arguments.putString(ARGUMENT_HIKE_PREVIEW, hikePreview);
        arguments.putString(ARGUMENT_CITY_ID, cityId);

        pageFragment.setArguments(arguments);


        return pageFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
            image = getArguments().getString(ARGUMENT_IMAGE_PATH);
            title = getArguments().getString(ARGUMENT_TITLE);
            hikeId = getArguments().getString(ARGUMENT_HIKE_ID);
            trackId = getArguments().getString(ARGUMENT_TRACK_ID);
            toPlay = getArguments().getBoolean(ARGUMENT_PLAY);
            reviewVersionFlag = getArguments().getBoolean(ARGUMENT_reviewVersionFlag);
            isHikeDownloaded = getArguments().getBoolean(ARGUMENT_IS_HIKE_DOWNLOADED);

            hikeTitle = getArguments().getString(ARGUMENT_HIKE_TITLE);
            hikePreview = getArguments().getString(ARGUMENT_HIKE_PREVIEW);
            cityId = getArguments().getString(ARGUMENT_CITY_ID);
        }

        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (btnPage != null) {
            setBtnPlay(btnPage);
        }
        if (getActivity() != null) {
            getActivity().registerReceiver(stopPlayerReceiver, new IntentFilter(Constants.BROADCAST_PLAYER_STATUS));
            getActivity().registerReceiver(broadcastBufferReceiver, new IntentFilter(Constants.BROADCAST_BUFFERING));
            getActivity().registerReceiver(downloadCompleteReceiver, new IntentFilter(Constants.BROADCAST_DOWNLOAD));
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        mUserVisibleHint = visible;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.no_image)
                .showImageOnFail(R.drawable.no_image)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_navigation, container, false);
        ImageView imgPage = view.findViewById(R.id.iv_navigation);

        if (SaveInSharedPref.isHikeDownloaded(hikeId)) {
            String sb = "file:///" + FileTools.getFolder() + "/" + hikeId + "/" + new File(image).getName();
            ImageLoader.getInstance().displayImage(sb, imgPage);
        } else {
            ImageLoader.getInstance().displayImage(App.getApp().getResources().getString(R.string.general)+ image, imgPage, options);
        }

        TextView tvPage = view.findViewById(R.id.tv_navigation);
        tvPage.setText(String.format(getResources().getString(R.string.placeholder_index), String.valueOf(pageNumber + 1), title));


        btnPage = view.findViewById(R.id.btn_navigation);

        setBtnPlay(btnPage);


        RelativeLayout rl = view.findViewById(R.id.rl_navigation);
        if (rl != null) {
            rl.setOnClickListener(view1 -> {
                Intent intent = new Intent(getContext(), PointExpListActivity.class);
                intent.putExtra(Constants.ID, hikeId);
                intent.putExtra(Constants.INDEX, pageNumber + 1);
                intent.putExtra(Constants.PLAY, toPlay);

                /*if (!isHikeDownloaded) {
                    intent.putExtra(Constants.HIKE_SOURCE, Constants.HIKE_FROM_CATALOG);
                    if (!reviewVersionFlag) {
                        intent.putExtra(Constants.INDEX, 1);
                    }
                } else {
                    intent.putExtra(Constants.HIKE_SOURCE, Constants.HIKE_DOWNLOADED);
                }*/
                startActivity(intent);
            });
        }
        downloadCompleteReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                int resCode = intent.getIntExtra(Constants.DOWNLOAD, Constants.DOWNLOAD_ERROR);
                String receivedHikeId = intent.getStringExtra(Constants.HIKE_ID);

                if (receivedHikeId != null) {
                    if (!receivedHikeId.isEmpty()) {
                        if (receivedHikeId.equals(hikeId)) {
                            switch (resCode) {
                                case Constants.DOWNLOADING:
                                    setBtnDownloading(btnPage);
                                    break;
                                case Constants.DOWNLOAD_COMPLETE:
                                    setBtnPlay(btnPage);
                                    break;
                                case Constants.DOWNLOAD_ERROR:
                                    setBtnToDownload(btnPage);
                                    break;
                            }
                        }
                    }
                }
            }
        };

        stopPlayerReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                if (Constants.playingHikeId.equals(hikeId)) {
                    if (Constants.playIndex == pageNumber) {
                        setBtnPlay(btnPage);
                    } else {
                        btnPage.setBackgroundResource(R.drawable.play);
                        hideSeek();
                    }
                }
            }
        };

        seekBar = view.findViewById(R.id.sb_play_progress_navigation);
        seekBar.setOnSeekBarChangeListener(this);
        seekBarIntent = new Intent(Constants.BROADCAST_SEEKBAR);
        SeekBarPosDispatcher.addListener(this);

        if (pageNumber == Constants.playIndex && hikeId.equals(Constants.playingHikeId)) {
            showSeek();
        } else {
            hideSeek();
        }

        return view;
    }

    private void setBtnToDownload(Button btn) {
        btn.setEnabled(true);
        btn.clearAnimation();
        btn.setBackgroundResource(R.drawable.ic_download);
        if (!StringRequests.isOnline()) {
            btn.setOnClickListener(view -> Toast.makeText(getContext(), R.string.check_connection, Toast.LENGTH_SHORT).show());
        }
    }

    private void setBtnDownloading(Button btn) {
        btn.setBackgroundResource(R.drawable.dowloading);
        btn.startAnimation(animation);
        btn.setEnabled(false);
    }

    private void setBtnPlay(Button btn) {
        btn.setEnabled(true);
        btn.clearAnimation();
        btn.setOnClickListener(SimplePlayListener.getInstance(hikeId, trackId, title, image, pageNumber, isHikeDownloaded));
        if (Constants.playingHikeId.equals(hikeId)) {
            if (Constants.playIndex == pageNumber) {
                if (!Constants.playerPaused) {
                    btnPage.setBackgroundResource(R.drawable.pause);
                } else {
                    btnPage.setBackgroundResource(R.drawable.play);
                }
            } else {
                btnPage.setBackgroundResource(R.drawable.play);
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null) {
            getActivity().unregisterReceiver(stopPlayerReceiver);
            if (downloadCompleteReceiver != null) {
                getActivity().unregisterReceiver(downloadCompleteReceiver);
            }
            if (broadcastBufferReceiver != null) {
                getActivity().unregisterReceiver(broadcastBufferReceiver);
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (b && getContext() != null) {
            int seekPos = seekBar.getProgress();
            seekBarIntent.putExtra("seekpos", seekPos);
            getContext().sendBroadcast(seekBarIntent);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void updatePosition(int seekProgress, int seekMax) {
        if (seekBar != null) {
            seekBar.setMax(seekMax);
            seekBar.setProgress(seekProgress);
        }
    }

    @Override
    public void hideSeek() {
        if (seekBar != null) {
            seekBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showSeek() {
        if (seekBar != null) {
            if (pageNumber == Constants.playIndex && hikeId.equals(Constants.playingHikeId)) {

                seekBar.setVisibility(View.VISIBLE);
                seekBar.setMax(AudioService.getMaxPosition());
                seekBar.setProgress(AudioService.getCurrentPosition());
            }
        }
    }

    private void showPD(Intent bufferIntent) {
        String bufferValue = bufferIntent.getStringExtra("buffering");
        int bufferIntValue = Integer.parseInt(bufferValue);
        switch (bufferIntValue) {
            case 0:
                if (pdBuff != null) {
                    pdBuff.dismiss();
                }
                break;
            case 1:
                bufferDialogue();
                break;
        }
    }

    private void bufferDialogue() {
        if (pdBuff == null)
            pdBuff = ProgressDialog.show(getActivity(), getString(R.string.buffering), getString(R.string.acquiring), true);
        else
            pdBuff.show();
        pdBuff.setCancelable(true);
        pdBuff.setOnCancelListener(dialogInterface -> {
            if (getActivity() != null)
                getActivity().stopService(new Intent(getActivity(), AudioService.class));
        });
    }
}
