package ru.cps.hike.screens.qr;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PointF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.base.BaseActivity;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.controller.http.StringRequests;
import ru.cps.hike.model.Hike;
import ru.cps.hike.model.ResultHike;
import ru.cps.hike.model.ResultPoints;
import ru.cps.hike.screens.map.MapFragment;
import ru.cps.hike.Constants;
import ru.cps.hike.screens.point.PointExpListActivity;

public class QRActivity extends BaseActivity implements QRCodeReaderView.OnQRCodeReadListener {

    public static final int CHECK_QR_LINK_RESULT_OK = 1;
    public static final int CHECK_QR_LINK_RESULT_ERROR = -1;
    public static final int CHECK_QR_LINK_RESULT_NOT_FIND = 0;
    private static final String TAG = "myLogs";
    protected BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };
    private QRCodeReaderView mydecoderview;
    private boolean showDialogFlag = true;
    private String hikeId;
    private String pointId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mydecoderview = findViewById(R.id.qrdecoderview);
        mydecoderview.setOnQRCodeReadListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(MapFragment.BROADCAST_ACTION_HIKE_ID);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void showDialogue(final String qrText) {

        String yes = getString(R.string.yes);
        AlertDialog.Builder dialog1 = new AlertDialog.Builder(this);
        dialog1.setNegativeButton(getString(R.string.no), (dialogInterface, i) -> dialogInterface.dismiss());
        dialog1.setCancelable(true);
        dialog1.setOnDismissListener(dialog -> showDialogFlag = true);
        dialog1.setOnCancelListener(dialog -> showDialogFlag = true);

        if (pointCode(qrText)) {

            if (StringRequests.isOnline()) {
                if (hikeId != null && pointId != null && !hikeId.isEmpty()) {

                    switch (checkQRLink(qrText)) {
                        case CHECK_QR_LINK_RESULT_OK: {
                            Intent intent = new Intent(this, PointExpListActivity.class);

                            intent.putExtra(Constants.ID, hikeId);
                            intent.putExtra(Constants.POINT_ID, pointId);

                            int hikeSource = Constants.HIKE_FROM_CATALOG;
                            if (SaveInSharedPref.isHikeDownloaded(hikeId)) {
                                hikeSource = Constants.HIKE_DOWNLOADED;
                            }
                            intent.putExtra(Constants.HIKE_SOURCE, hikeSource);

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            break;
                        }
                        case CHECK_QR_LINK_RESULT_NOT_FIND:
                            setNegativeButton(dialog1, yes, getString(R.string.hike_not_found));
                            dialog1.show();
                            break;
                        case CHECK_QR_LINK_RESULT_ERROR:
                            setNegativeButton(dialog1, yes, getString(R.string.unable_to_open_hike));
                            dialog1.show();
                            break;
                        default:
                            setNegativeButton(dialog1, yes, getString(R.string.unable_to_open_hike));
                    }


                } else {
                    dialog1.setTitle(getString(R.string.error_link_qu));
                    dialog1.setNegativeButton(getString(R.string.yes), (dialogInterface, i) -> dialogInterface.dismiss());
                    dialog1.show();
                }
            } else {
                if (hikeId != null && pointId != null && !hikeId.isEmpty()) {
                    Intent intent = new Intent(this, PointExpListActivity.class);

                    intent.putExtra(Constants.ID, hikeId);
                    intent.putExtra(Constants.POINT_ID, pointId);

                    int hikeSource;
                    if (SaveInSharedPref.isHikeDownloaded(hikeId)) {
                        hikeSource = Constants.HIKE_DOWNLOADED;
                        intent.putExtra(Constants.HIKE_SOURCE, hikeSource);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        dialog1.setTitle(getString(R.string.hike_not_downloaded_qu_title));
                        dialog1.setMessage(getString(R.string.hike_not_downloaded_qu_message));
                        dialog1.setNegativeButton(getString(R.string.yes), (dialogInterface, i) -> dialogInterface.dismiss());
                        dialog1.show();
                    }
                } else {
                    dialog1.setTitle(getString(R.string.error_link_qu));
                    dialog1.setNegativeButton(getString(R.string.yes), (dialogInterface, i) -> dialogInterface.dismiss());
                    dialog1.show();
                }
            }
        } else if (mayBeALink(qrText)) {
            dialog1.setTitle(getString(R.string.open_link_qu));
            dialog1.setMessage(qrText);
            dialog1.setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(qrText));
                startActivity(intent);
            });

            dialog1.show();
        } else {
            dialog1.setMessage(qrText);
            dialog1.setNegativeButton(getString(R.string.yes), (dialogInterface, i) -> dialogInterface.dismiss());
            dialog1.show();
        }
    }

    private void setNegativeButton(AlertDialog.Builder dialog, String buttonLabel, String message) {
        dialog.setMessage(message);
        dialog.setNegativeButton(buttonLabel, (dialogInterface, i) -> dialogInterface.dismiss());
    }

    private boolean pointCode(String qrText) {
        if (qrText != null && !qrText.isEmpty()) {
            String parseLine = qrText.replace('#', 'a');
            Uri uri = Uri.parse(parseLine);
            Uri uriHost = Uri.parse(parseLine);
            if (uriHost == null || uri == null || uriHost.getHost() == null) {
                return false;
            }
            Log.w(TAG, "pointCode: " + qrText );
            String patternGen = "^https?://"+App.getApp().getResources().getString(R.string.general).split(":")[1].substring(2, App.getApp().getResources().getString(R.string.general).split(":")[1].length())+"(:80|:443)?/#/app?";
         //   String patternGen = "^https?://tripgis.ru(:80|:443)?/#/app?";
            Log.w("myLog", "pointCode: " + patternGen );

            Pattern patternGeneral = Pattern.compile(patternGen);
            Pattern patternShowhike = Pattern.compile("showhike");
            Pattern patternShowpoint = Pattern.compile("showpoint");

            Matcher matcherGeneral = patternGeneral.matcher(qrText);
            Matcher matcherShowhike = patternShowhike.matcher(qrText);
            Matcher matcherShowpoint = patternShowpoint.matcher(qrText);

            if (matcherGeneral.find() && matcherShowhike.find() && matcherShowpoint.find()) {
                hikeId = uri.getQueryParameter("showhike");
                pointId = uri.getQueryParameter("showpoint");
                return true;
            }
        }
        return false;
    }

    private int checkQRLink(final String qrText) {
        int res = CHECK_QR_LINK_RESULT_ERROR;
        class CheckLinkTask extends AsyncTask<String, Void, Integer> {
            private String line = "";
            private ProgressDialog pdBuff = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (pdBuff == null)
                    pdBuff = ProgressDialog.show(QRActivity.this, getString(R.string.getting_data), "", true);
                else
                    pdBuff.show();
            }

            @Override
            protected void onPostExecute(Integer aBoolean) {
                super.onPostExecute(aBoolean);
                if (pdBuff != null)
                    pdBuff.dismiss();
            }

            @Override
            protected Integer doInBackground(String... urls) {
                if (qrText != null) {

                    String parseLine = null;
                    if (!qrText.isEmpty()) {
                        parseLine = qrText.replace('#', 'a');
                    }
                    Uri uri = Uri.parse(parseLine);
                    hikeId = uri.getQueryParameter("showhike");
                    pointId = uri.getQueryParameter("showpoint");
                    if (!SaveInSharedPref.isHikeDownloaded(hikeId)) {
                        int hikeResult = getHikeFromServer(hikeId);
                        int pointsResult = getPointsFromServer(hikeId);

                        if (hikeResult == CHECK_QR_LINK_RESULT_OK && pointsResult == CHECK_QR_LINK_RESULT_OK) {
                            return CHECK_QR_LINK_RESULT_OK;
                        } else if (hikeResult == CHECK_QR_LINK_RESULT_NOT_FIND) {
                            return CHECK_QR_LINK_RESULT_NOT_FIND;
                        } else {
                            return CHECK_QR_LINK_RESULT_ERROR;
                        }
                    } else {
                        return CHECK_QR_LINK_RESULT_OK;
                    }

                } else {
                    return CHECK_QR_LINK_RESULT_ERROR;
                }
            }

            private int getHikeFromServer(String hikeId) {
                int result = CHECK_QR_LINK_RESULT_ERROR;
                if (hikeId != null && !hikeId.isEmpty()) {
                    int resCode;
                    InputStream in;
                    BufferedReader r;
                    HttpURLConnection connection;


                    if (!SaveInSharedPref.isHikeDownloaded(hikeId)) {
                        try {
                            URL url = new URL(StringRequests.getOneHikeRequest(hikeId));
                            connection = (HttpURLConnection) url.openConnection();
                            connection.setAllowUserInteraction(false);
                            connection.setInstanceFollowRedirects(true);
                            connection.setRequestMethod("GET");
                            connection.connect();

                            resCode = connection.getResponseCode();
                            if (resCode == HttpURLConnection.HTTP_OK) {
                                in = connection.getInputStream();
                                r = new BufferedReader(new InputStreamReader(in));
                                StringBuilder total = new StringBuilder();

                                while ((line = r.readLine()) != null) {
                                    total.append(line);
                                }
                                line = total.toString();

                                ResultHike hike = new Gson().fromJson(line, ResultHike.class);
                                if (hike != null) {
                                    if (hikeId.equals(hike.getData().get(0).getId())) {
                                        FileTools.saveExport(new Gson().toJson(hike.getData().get(0)), hikeId);
                                        result = CHECK_QR_LINK_RESULT_OK;
                                    } else {
                                        result = CHECK_QR_LINK_RESULT_NOT_FIND;
                                    }
                                } else {
                                    result = CHECK_QR_LINK_RESULT_NOT_FIND;
                                }
                            }
                        } catch (Exception ignored) {
                        }
                    } else {
                        result = CHECK_QR_LINK_RESULT_OK;
                    }
                }
                return result;
            }

            private int getPointsFromServer(String hikeId) {
                int result = CHECK_QR_LINK_RESULT_ERROR;

                String line;

                if (!SaveInSharedPref.isHikeDownloaded(hikeId)) {

                    InputStream in = null;
                    BufferedReader r = null;
                    HttpURLConnection connection = null;
                    int resCode;

                    String link = StringRequests.pointsRequest(hikeId);
                    if (StringRequests.isOnline()) {
                        try {
                            URL url = new URL(link);
                            connection = (HttpURLConnection) url.openConnection();
                            connection.setAllowUserInteraction(false);
                            connection.setInstanceFollowRedirects(true);
                            connection.setRequestMethod("GET");
                            connection.connect();
                            resCode = connection.getResponseCode();

                            if (resCode == HttpURLConnection.HTTP_OK) {
                                in = connection.getInputStream();
                                r = new BufferedReader(new InputStreamReader(in));
                                StringBuilder total = new StringBuilder();
                                while ((line = r.readLine()) != null) {
                                    total.append(line);
                                }
                                line = total.toString();
                                Log.w(TAG, "getPointsFromServer: " + line );
                                ResultPoints resultPoints = new Gson().fromJson(line, ResultPoints.class);
                                if (resultPoints != null && resultPoints.getData().size() > 0) {
                                    FileTools.savePoints(line, hikeId);
                                    result = CHECK_QR_LINK_RESULT_OK;
                                } else {
                                    result = CHECK_QR_LINK_RESULT_NOT_FIND;
                                }
                            }
                        } catch (IOException ignored) {
                        } finally {
                            if (in != null) try {
                                in.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (connection != null) connection.disconnect();
                            if (r != null) try {
                                r.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                return result;
            }
        }

        try {
            res = new CheckLinkTask().execute(qrText).get(10, TimeUnit.SECONDS);
        } catch (Exception ignored) {
        }

        return res;
    }

    private boolean mayBeALink(String link) {
        Pattern p = Pattern.compile("(http)|www");
        Matcher m = p.matcher(link);
        return m.find();
    }

    @Override
    public void onQRCodeRead(String s, PointF[] pointFs) {
        if (s != null) {
            if (showDialogFlag) {
                showDialogue(s);
                showDialogFlag = false;
            }
        }
    }


    @Override
    public void cameraNotFound() {
    }

    @Override
    public void QRCodeNotFoundOnCamImage() {
    }


    @Override
    protected void onResume() {
        super.onResume();

        mydecoderview.getCameraManager().startPreview();
        showDialogFlag = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "unregisterReceiver(br) IllegalArgumentException");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mydecoderview.getCameraManager().stopPreview();
    }
}