package ru.cps.hike.screens.point;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.cps.hike.App;
import ru.cps.hike.Constants;
import ru.cps.hike.R;
import ru.cps.hike.base.BasePresenter;
import ru.cps.hike.connection.Api;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.dagger.module.LocalStorage;
import ru.cps.hike.model.Hike;
import ru.cps.hike.model.Point;

@InjectViewState
public class PointExpListPresenter extends BasePresenter<PointExpListView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;
    private String hikeId;
    private Hike hike;
    private int index;
    private String pointId;

    void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Hike getHike() {
        return hike;
    }

    public void setHikeId(String hikeId) {
        this.hikeId = hikeId;
    }

    public String getHikeId() {
        return hikeId;
    }

    PointExpListPresenter() {
        App.getAppComponent().inject(this);
    }

    void loadHike() {
        String hikeJson = FileTools.readExportFile(hikeId);
        hike = new Gson().fromJson(hikeJson, Hike.class);

        Map<String, String> query = new HashMap<>();
        query.put("lang", App.getCurrentLang());
        Disposable disposable = mApi.getHikeSize(hike.getId(), query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            hike.setSize(result.getSize());
                            getViewState().notifyList();

                        }
                        , throwable -> {
                        });
        unsubscribeOnDestroy(disposable);

    }


    private int findPointIndexById(List<Point> points) {
        if (pointId != null && !pointId.isEmpty() && points != null && points.size() > 0) {
            for (int i = 0; i < points.size(); i++) {
                if (pointId.equals(points.get(i).getId())) {
                    return i + 1;
                }
            }
        }
        return 0;
    }

    void updateList() {
        if (!SaveInSharedPref.isHikeDownloaded(hikeId)) {
            Map<String, String> query = new HashMap<>();
            query.put("hike", hikeId);
            query.put("lang", App.getCurrentLang());

            Disposable disposable = mApi.getPoints(query)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result2 -> {
                                hike.setPoints(result2.getData());
                                if (!TextUtils.isEmpty(pointId)) {
                                    index = findPointIndexById(hike.getPoints());
                                }
                                FileTools.saveExport(new Gson().toJson(hike), hike.getId());
                                getViewState().notifyList();
                                if (index > 0)
                                    getViewState().openGroup(index);
                            }
                            , throwable -> {
                                Point showNoInernetMessage = new Point();
                                showNoInernetMessage.getLang().setDescription(App.getApp().getResources().getString(R.string.for_view_need_internet));
                                List<Point> pointList = new ArrayList<>();
                                pointList.add(showNoInernetMessage);
                                hike.setPoints(pointList);
                                getViewState().notifyList();
                            });
            unsubscribeOnDestroy(disposable);
        } else {
            String s = FileTools.readExportFile(hikeId);
            Hike h = new Gson().fromJson(s, Hike.class);
            hike.setPoints(h.getPoints());
            getViewState().notifyList();
            if (!TextUtils.isEmpty(pointId)) {
                index = findPointIndexById(hike.getPoints());
            }
            if (index > 0)
                getViewState().openGroup(index);
        }
    }
}
