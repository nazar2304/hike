package ru.cps.hike.screens.point;

import ru.cps.hike.base.BaseView;

public interface PointExpListView extends BaseView {
    void notifyList();
    void openGroup(int index);
}
