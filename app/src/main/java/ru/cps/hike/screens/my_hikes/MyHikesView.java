package ru.cps.hike.screens.my_hikes;

import ru.cps.hike.base.BaseView;

public interface MyHikesView extends BaseView {
    void notifyList();
}
