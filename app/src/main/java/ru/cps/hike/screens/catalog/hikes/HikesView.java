package ru.cps.hike.screens.catalog.hikes;

import ru.cps.hike.base.BaseView;

public interface HikesView extends BaseView {

    void showEmpty(boolean status);

    void setHikesTitle(String country, String city);

    void notifyList();
}
