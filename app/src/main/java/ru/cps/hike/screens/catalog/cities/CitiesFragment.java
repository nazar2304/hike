package ru.cps.hike.screens.catalog.cities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.base.BaseFragment;
import ru.cps.hike.model.City;
import ru.cps.hike.Constants;
import ru.cps.hike.screens.catalog.hikes.HikesFragment;
import ru.cps.hike.screens.catalog.BaseContainerFragment;

public class CitiesFragment extends BaseFragment implements CitiesView {

    @InjectPresenter CitiesPresenter mPresenter;
    @BindView(R.id.listView) ListView mListView;
    @BindView(R.id.empty) View mEmpty;
    @BindView(R.id.citiesTitle) TextView mCitiesTitle;
    private CityAdapter adapter;
    private String tabTag;
    private boolean toUpdateList = true;
    private boolean firstLaunch = true;


    private BroadcastReceiver onLineReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null) {
                    boolean isConn = netInfo.isConnectedOrConnecting();
                    if (toUpdateList) {
                        if (isConn) {
                            mPresenter.load();
                            toUpdateList = false;
                        }
                    }
                }
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cities, container, false);
        unbinder = ButterKnife.bind(this, view);
        progress = view.findViewById(R.id.progress);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mPresenter.setCountry(bundle.getString(Constants.COUNTRY, "no argument pass"), bundle.getString(Constants.COUNTRY_ID, "no argument pass"), bundle.getInt(Constants.CITY, 0));
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView.setDivider(null);
        mListView.setOnItemClickListener((parent, view1, position, id) -> {
            City c = adapter.getCity(position);

            if (c.getHikesCount() > 0) {
                HikesFragment hikesFragment = new HikesFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.CITY, c.getLang().getName());
                bundle.putString(Constants.CITY_ID, c.getId());
                bundle.putString(Constants.COUNTRY, mPresenter.getCountry());
                hikesFragment.setTabTag(tabTag);
                hikesFragment.setArguments(bundle);

                if (getParentFragment() != null)
                    ((BaseContainerFragment) getParentFragment()).replaceFragment(hikesFragment, true);
            } else {
                Toast.makeText(App.getApp(), R.string.no_hikes, Toast.LENGTH_SHORT).show();
            }
        });
        adapter = new CityAdapter(getContext(), mPresenter.getCityList());
        mListView.setAdapter(adapter);
        if (firstLaunch) {
            mPresenter.load();
            firstLaunch = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter onLineFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        if (getActivity() != null)
            getActivity().registerReceiver(onLineReceiver, onLineFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null)
            getActivity().unregisterReceiver(onLineReceiver);
    }

    public void setTabTag(String tabTag) {
        this.tabTag = tabTag;
    }

    @Override
    public void setCitiesTitle(String country, int count) {
        mCitiesTitle.setText(String.format(getResources().getString(R.string.placeholder_cities_title), getResources().getString(R.string.cities), country, count));
    }

    @Override
    public void showEmpty(boolean status) {
        mEmpty.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    @Override
    public void notifyList() {
        adapter.notifyDataSetChanged();
    }
}
