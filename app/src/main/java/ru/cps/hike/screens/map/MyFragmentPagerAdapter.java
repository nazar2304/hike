package ru.cps.hike.screens.map;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ru.cps.hike.model.Hike;

public class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private boolean isHikeDownloaded;
    private Hike hike;


    MyFragmentPagerAdapter(FragmentManager fragmentManager, Hike hike, boolean isHikeDownloaded) {
        super(fragmentManager);
        this.hike = hike;
        this.isHikeDownloaded = isHikeDownloaded;
    }


    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(position, hike.getPoints().get(position), false, isHikeDownloaded, hike.getLang().getTitle(), hike.getImage().getPreview(), hike.getCityId(), hike.getId());
    }

    @Override
    public int getCount() {
        return hike.getPoints().size();
    }
}
