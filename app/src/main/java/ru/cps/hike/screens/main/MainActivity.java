package ru.cps.hike.screens.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import ru.cps.hike.R;
import ru.cps.hike.controller.audio.AudioService;
import ru.cps.hike.screens.preferences.Preferences;
import ru.cps.hike.screens.catalog.BaseContainerFragment;
import ru.cps.hike.screens.catalog.TabContainerFragment;
import ru.cps.hike.screens.map.MapFragment;
import ru.cps.hike.Constants;
import ru.cps.hike.screens.my_hikes.MyHikesFragment;
import ru.cps.hike.screens.policy.PolicyActivity;
import ru.cps.hike.screens.qr.QRActivity;


public class MainActivity extends AppCompatActivity {
    private BroadcastReceiver br;
    private Intent mapLaunch;
    private FragmentTabHost mTabHost;

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.threadPoolSize(3);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(200 * 1024 * 1024);
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs();
        ImageLoader.getInstance().init(config.build());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        br = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                int mapStart = intent.getIntExtra(Constants.MAP_START, 0);
                SharedPreferences prefs = getSharedPreferences("MAIN_PREFS" + getPackageName(), MODE_PRIVATE);
                SharedPreferences.Editor ed = prefs.edit();
                ed.putInt("TAB_INDEX", 0);
                ed.apply();
                intent.setAction(MapFragment.HIKE_ID);
                mapLaunch = intent;

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                SharedPreferences.Editor edit = sp.edit();
                edit.putString(MapFragment.HIKE_ID, intent.getStringExtra(MapFragment.HIKE_ID));
                edit.putInt(MapFragment.POINT_POSITION, intent.getIntExtra(MapFragment.POINT_POSITION, 0));
                edit.apply();
                if (mapStart == Constants.MAP_START_FROM_TAB) {
                    mTabHost.setCurrentTab(0);
                }
            }
        };
        initImageLoader(this);
        defDisplaySize();
        Preferences preferences = new Preferences(getApplicationContext());
        if (!preferences.getBoolean(Preferences.ACCEPTED_POLICY)) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setView(View.inflate(getApplicationContext(), R.layout.dialog_policy, null))
                    .setPositiveButton(getString(R.string.policy_decline), (dialogInterface, i) -> finish())
                    .setNegativeButton(getString(R.string.policy_accept), (dialogInterface, i) ->
                            preferences.putBoolean(Preferences.ACCEPTED_POLICY, true))
                    .setCancelable(false)
                    .create();
            alertDialog.setOnShowListener((DialogInterface dialogInterface) -> {
                String policy = getString(R.string.policy);
                SpannableString spannableString = new SpannableString(policy.replace("*", ""));
                spannableString.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View view) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://files.1cps.ru/mobile/policies/audioguide/index.html")));
                    }
                }, policy.indexOf("*"), spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                TextView title = alertDialog.findViewById(R.id.title);
                title.setText(spannableString);
                title.setMovementMethod(LinkMovementMethod.getInstance());
            });
            alertDialog.show();
        }
        Permissions.check(this, new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, null, new Permissions.Options(), new PermissionHandler() {
            @Override
            public void onGranted() {
            }
        });
    }



    private void defDisplaySize() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Constants.screenWidth = metrics.widthPixels;
        Constants.screenHeight = metrics.heightPixels;

    }


    private void initView() {
        SharedPreferences prefs = getSharedPreferences("MAIN_PREFS" + getPackageName(), MODE_PRIVATE);

        mTabHost = findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        mTabHost.addTab(mTabHost.newTabSpec(getResources().getString(R.string.title_map)).setIndicator(getResources().getString(R.string.title_map)), MapFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(getResources().getString(R.string.title_catalog)).setIndicator(getResources().getString(R.string.title_catalog)), TabContainerFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(getResources().getString(R.string.title_my)).setIndicator(getResources().getString(R.string.title_my)), MyHikesFragment.class, null);
        mTabHost.setBackgroundColor(getResources().getColor(R.color.actionbar_color));

        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            final TextView tv = mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            mTabHost.getTabWidget().getChildAt(i).setBackground(getResources().getDrawable(R.drawable.bg_tab_selector));
            if (i > 0) {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().show();
                }
            }

            if (tv == null)
                continue;
            else {
                tv.setTextSize(getResources().getDimension(R.dimen.tab_title_text_size));
                tv.setAllCaps(false);
                tv.setTextColor(getResources().getColor(R.color.actionbar_text_color));
            }
            mTabHost.getTabWidget().setDividerDrawable(null);
        }
        mTabHost.setCurrentTab(prefs.getInt("TAB_INDEX", 1));

        if (getSupportActionBar() != null)
            getSupportActionBar().setElevation(0);

    }

    @Override
    public void onBackPressed() {
        boolean isPopFragment = false;
        String currentTabTag = mTabHost.getCurrentTabTag();
        if (currentTabTag != null && currentTabTag.equals(getResources().getString(R.string.title_catalog))) {
            BaseContainerFragment baseContainerFragment = ((BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(getResources().getString(R.string.title_catalog)));
            if (baseContainerFragment != null)
                isPopFragment = baseContainerFragment.popFragment();
        }

        if (!isPopFragment) {
            closeApp();
        }

    }

    private void closeApp() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.close_app));
        dialog.setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> finish());
        dialog.setNegativeButton(getString(R.string.no), (dialogInterface, i) -> dialogInterface.dismiss());
        dialog.setCancelable(true);
        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveMainPrefs();
    }

    private void saveMainPrefs() {
        SharedPreferences prefs = getSharedPreferences("MAIN_PREFS" + getPackageName(), MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putInt("TAB_INDEX", mTabHost.getCurrentTab());
        ed.apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveMainPrefs();
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences("MAIN_PREFS" + getPackageName(), MODE_PRIVATE);
        mTabHost.setCurrentTab(prefs.getInt("TAB_INDEX", 0));
        if (mapLaunch != null) {
            sendBroadcast(mapLaunch);
            mapLaunch = null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(MapFragment.BROADCAST_ACTION_HIKE_ID);
        registerReceiver(br, intentFilter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_qr:
                Permissions.check(this, Manifest.permission.CAMERA, null, new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        Intent intent = new Intent(MainActivity.this, QRActivity.class);
                        startActivity(intent);
                    }
                });
                break;
            case R.id.menu_about:
                Intent intent = new Intent(this, PolicyActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(br);
        stopService(new Intent(this, AudioService.class));

    }
}