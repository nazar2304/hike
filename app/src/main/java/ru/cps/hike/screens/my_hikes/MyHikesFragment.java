package ru.cps.hike.screens.my_hikes;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.cps.hike.R;
import ru.cps.hike.base.BaseFragment;
import ru.cps.hike.controller.dowload_hike_listener.DownloadHikeDispatcher;
import ru.cps.hike.controller.dowload_hike_listener.IDownloadStatusListener;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;
import ru.cps.hike.screens.point.PointExpListActivity;


public class MyHikesFragment extends BaseFragment implements IDownloadStatusListener, MyHikesView {

    @InjectPresenter MyHikesPresenter mPresenter;
    @BindView(R.id.listView) ListView mListView;
    @BindView(R.id.empty) View mEmpty;
    HikeAdapter adapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DownloadHikeDispatcher.addListener(this);
        mPresenter.load();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_hikes, container, false);
        unbinder = ButterKnife.bind(this, view);
        Constants.toUpdateMyHikesFragment = false;
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView.setOnItemClickListener((parent, view1, position, id) -> {
            Hike h = adapter.getHike(position);
            Intent intent = new Intent(getContext(), PointExpListActivity.class);
            intent.putExtra(Constants.ID, h.getId());
            intent.putExtra(Constants.HIKE_SOURCE, Constants.HIKE_DOWNLOADED);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constants.VISIBILITY, false);
            startActivity(intent);
        });
        if (getContext() != null) {
            adapter = new HikeAdapter(getContext(), mPresenter.getHikes());
            mListView.setAdapter(adapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constants.toUpdateMyHikesFragment && getFragmentManager() != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
            Constants.toUpdateMyHikesFragment = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ImageLoader.getInstance().clearMemoryCache();
    }

    @Override
    public void onDownloadComplete(int resCode, String hikeId) {
        Constants.toUpdateMyHikesFragment = true;
        mPresenter.load();
    }

    @Override
    public void notifyList() {
        adapter.notifyDataSetChanged();
    }
}
