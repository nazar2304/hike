package ru.cps.hike.screens.map;


import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.cachemanager.CacheManager;
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer;
import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.bonuspack.overlays.Polyline;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.tileprovider.MapTileProviderBasic;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.MapBoxTileSource;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.NetworkLocationIgnorer;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.DirectedLocationOverlay;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.cps.hike.R;
import ru.cps.hike.base.BaseFragment;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.helpers.ImageHelper;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;
import ru.cps.hike.model.Point;


public class MapFragment extends BaseFragment implements LocationListener {

    public static final String HIKE_ID = "hikeId";
    public static final String POINT_ID = "pointId";
    public static final String POINT_POSITION = "point_position";
    public final static String BROADCAST_ACTION_HIKE_ID = "ru.cps.hike.hikeId";
    static final float POINT_ALPHA = 0.7f;
    final static Handler handler = new Handler();
    static float DEF_LATITUDE = 50.595f, DEF_LONGITIDE = 36.589f;
    final int PULSE_INTERVAL = 20;
    @BindView(R.id.map) protected GenericMapView genericMap;
    @BindView(R.id.zoomTV) protected TextView zoomTV;
    @BindView(R.id.btn_plus) protected ImageButton btnPlus;
    @BindView(R.id.btn_minus) protected ImageButton btnMinus;
    @BindView(R.id.imgbtn_location) protected ImageButton imgbtnLocation;
    @BindView(R.id.btn_mapmode) protected ImageButton btnMapMode;
    @BindView(R.id.vp_navigation) protected ViewPager vpNavigation;
    ImageView animateView;
    protected MapView map;
    protected IMapController mapController;
    protected GeoPoint startPoint;
    protected int zoom;
    protected RadiusMarkerClusterer hikesMarks;
    protected RadiusMarkerClusterer myHikesMarks;
    protected RadiusMarkerClusterer pointMarkers;
    protected LocationManager mLocationManager;
    protected DirectedLocationOverlay myLocationOverlay;
    protected boolean mTrackingMode;
    protected MyFragmentPagerAdapter pagerAdapter;
    protected BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            hikeId = intent.getStringExtra(HIKE_ID);
            pointPosition = intent.getIntExtra(POINT_POSITION, 0);
            startFromOnRecieve = true;
            initVpNavigation(hikeId, pointPosition);
        }
    };
    protected String hikeId;
    protected int pointPosition;
    int mWhichRouteProvider;
    boolean startFromOnRecieve = false;
    int hikeSource;
    boolean isHikeDownloaded;
    List<Point> pointsList;
    boolean alphaDirection;
    int currentOverlayAlpha;
    OverlayItem overlayItem;
    ItemizedIconOverlay<OverlayItem> overlayIcon;
    boolean trackShown;
    ArrayList<GeoPoint> track;
    Polyline lineTrek;
    Animation animation;
    Runnable hidingDelay;
    int oldPosition = -1;
    Timer timer;
    TimerTask timerTask;
    Runnable runnable;
    long mLastTime = 0;
    float mAzimuthAngleSpeed = 0.0f;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        IntentFilter intentFilter = new IntentFilter(HIKE_ID);
        if (getActivity() != null)
            getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if ((savedInstanceState != null)) {
            startPoint = new GeoPoint(savedInstanceState.getFloat("MAP_CENTER_LAT", DEF_LATITUDE), savedInstanceState.getFloat("MAP_CENTER_LON", DEF_LONGITIDE));
            zoom = savedInstanceState.getInt("zoom", 12);
        } else {
            zoom = prefs.getInt("MAP_ZOOM_LEVEL", 13);
            startPoint = new GeoPoint((double) prefs.getFloat("MAP_CENTER_LAT", DEF_LATITUDE), (double) prefs.getFloat("MAP_CENTER_LON", DEF_LONGITIDE));
        }

        MapTileProviderBasic bitmapProvider = new MapTileProviderBasic(getActivity().getApplicationContext());
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        unbinder = ButterKnife.bind(this, view);

        genericMap.setTileProvider(bitmapProvider);
        map = genericMap.getMapView();
        mapController = map.getController();
        map.setMultiTouchControls(true);
        mapController.setZoom(zoom);
        mapController.setCenter(startPoint);

        ScaleBarOverlay scaleBarOverlay = new ScaleBarOverlay(map);
        map.getOverlays().add(scaleBarOverlay);

        map.setMapListener(new MapListener() {
            @Override
            public boolean onScroll(ScrollEvent event) {
                zoomTV.setText(String.valueOf(map.getZoomLevel()));
                return false;
            }

            @Override
            public boolean onZoom(ZoomEvent event) {
                zoomTV.setText(String.valueOf(map.getZoomLevel()));
                return false;
            }
        });

        map.setOnTouchListener((v, motionEvent) -> {
            showWidgets();
            hideWidgets();
            return false;
        });
        map.setLongClickable(true);
        map.setOnLongClickListener(v -> {
            hideWidgets(1);
            return false;
        });

        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        myLocationOverlay = new DirectedLocationOverlay(getActivity());
        map.getOverlays().add(myLocationOverlay);


        String tileProviderName = prefs.getString("TILE_PROVIDER", "Mapnik");
        try {
            ITileSource tileSource = TileSourceFactory.getTileSource(tileProviderName);
            map.setTileSource(tileSource);
        } catch (IllegalArgumentException e) {
            map.setTileSource(TileSourceFactory.MAPNIK);
        }
        btnPlus.setOnClickListener(v -> mapController.zoomIn());
        btnMinus.setOnClickListener(v -> mapController.zoomOut());


        if (savedInstanceState == null) {
            try {
                Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location == null)
                    location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    onLocationChanged(location);
                } else {
                    myLocationOverlay.setEnabled(false);
                }
            } catch (SecurityException e) {
                myLocationOverlay.setEnabled(false);
            }
            startPoint = null;
        } else {
            myLocationOverlay.setLocation(savedInstanceState.getParcelable("location"));

        }

        imgbtnLocation.setOnClickListener(v -> {
            if (myLocationOverlay.getLocation() != null) {
                startLocationUpdates();
                map.getController().animateTo(myLocationOverlay.getLocation());
            } else
                Toast.makeText(getActivity(), getString(R.string.location_service_unavailable), Toast.LENGTH_LONG).show();
        });

        if (mTrackingMode) btnMapMode.setBackgroundResource(R.drawable.ic_compas);
        else btnMapMode.setBackgroundResource(R.drawable.ic_map);

        btnMapMode.setOnClickListener(v -> {
            mTrackingMode = !mTrackingMode;
            if (mTrackingMode) {
                btnMapMode.setBackgroundResource(R.drawable.ic_compas);
                startLocationUpdates();
                if (myLocationOverlay != null) {
                    myLocationOverlay.setBearing(0.0f);
                }
            } else {
                btnMapMode.setBackgroundResource(R.drawable.ic_map);
                startLocationUpdates();
                map.setMapOrientation(0.0f);
            }
        });


        hikeId = prefs.getString(HIKE_ID, null);
        pointPosition = prefs.getInt(POINT_POSITION, 0);
        hikeSource = prefs.getInt(Constants.HIKE_SOURCE, Constants.HIKE_FROM_CATALOG);

        if (hikeId != null) {
            initVpNavigation(hikeId, pointPosition);
        }

        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.alpha_test);
        if (animateView == null)
            animateView = getAnimateView();

        return view;
    }

    private void initVpNavigation(final String gotHikeId, final int pointIndex) {
        if (gotHikeId != null) {

            isHikeDownloaded = SaveInSharedPref.isHikeDownloaded(gotHikeId);

            String res = FileTools.readExportFile(gotHikeId);
            Hike hike = new Gson().fromJson(res, Hike.class);

            if (hike != null) {
                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                if (actionBar != null) {
                    String hikeTitleForAB = hike.getLang().getTitle();
                    if (hikeTitleForAB != null) {
                        actionBar.setTitle(hikeTitleForAB);
                    }
                }
                if (hike.getId() != null) {
                    pointsList = hike.getPoints();
                    pagerAdapter = new MyFragmentPagerAdapter(getChildFragmentManager(), hike, isHikeDownloaded);
                    vpNavigation.setAdapter(pagerAdapter);
                    vpNavigation.setClipToPadding(false);
                    vpNavigation.setPadding(30, 0, 30, 0);
                    vpNavigation.setPageMargin(10);
                    vpNavigation.setCurrentItem(pointIndex);
                    map.getController().animateTo(new GeoPoint(pointsList.get(pointIndex).getX(), pointsList.get(pointIndex).getY()));

                    vpNavigation.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                        @Override
                        public void onPageSelected(int position) {
                            setActiveMarker(position);
                            pointPosition = position;

                            if (Constants.toPlay) {

                                if (position != Constants.playIndex && Constants.playIndex > -1) {
                                    Intent intent = new Intent(Constants.BROADCAST_NEXT_TRACK);
                                    intent.putExtra(Constants.ID, hikeId);
                                    intent.putExtra(Constants.AUDIO, pointsList.get(position).getLang().getAudio());
                                    intent.putExtra(Constants.IMAGE, pointsList.get(position).getImages().get(0).getPreview());
                                    intent.putExtra(Constants.TITLE, pointsList.get(position).getLang().getTitle());
                                    intent.putExtra(Constants.INDEX, position);
                                }
                            }
                        }

                        @Override
                        public void onPageScrolled(int position, float positionOffset,
                                                   int positionOffsetPixels) {
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {
                        }
                    });
                    hikeOverlay(pointIndex);
                }
            }
        }


    }

    private void hikeOverlay(int pointIndex) {
        clearHikesOverlay();

        pointMarkers = new RadiusMarkerClusterer(getActivity().getApplicationContext());
        pointMarkers.setRadius(50);

        Drawable clusterIconD = getResources().getDrawable(R.drawable.marker_cluster1);
        Bitmap clusterIcon = ((BitmapDrawable) clusterIconD).getBitmap();
        pointMarkers.setIcon(clusterIcon);

        pointMarkers.getTextPaint().setTextSize(16.0f);
        map.getOverlays().add(pointMarkers);

        if (pointsList != null) {
            for (int i = 0; i < pointsList.size(); i++) {
                PointMarker m = new PointMarker(map);

                if (!isHikeDownloaded) {
                    if (pointsList.get(i).getReviewVersionFlag()) {

                        m.setIcon(ImageHelper.drawTextToBitmap(getActivity(), R.drawable.pointsolid, "" + (i + 1)));
                        m.setAlpha(POINT_ALPHA);
                        m.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    } else {
                        m.setIcon(getActivity().getResources().getDrawable(R.drawable.ic_to_download));
                        m.setAlpha(POINT_ALPHA);
                        m.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);

                    }
                } else {
                    m.setIcon(ImageHelper.drawTextToBitmap(getActivity(), R.drawable.pointsolid, "" + (i + 1)));
                    m.setAlpha(POINT_ALPHA);
                    m.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                }

                final GeoPoint markerPoint = new GeoPoint(pointsList.get(i).getY(), pointsList.get(i).getX());
                m.setPosition(markerPoint);
                m.setId(pointsList.get(i).getId());
                m.setTitle(pointsList.get(i).getLang().getTitle());
                final int finalI = i;

                m.setOnMarkerClickListener((marker, mapView) -> {
                    vpNavigation.setCurrentItem(finalI);
                    setActiveMarker(finalI);
                    return false;
                });

                m.setInfoWindow(null);
                m.setTitle(pointsList.get(i).getLang().getTitle());
                pointMarkers.add(m);
            }
            setActiveMarker(pointIndex);
        } else {
            Toast.makeText(getActivity(), "Hike reading error", Toast.LENGTH_SHORT).show();
        }
        map.invalidate();
    }

    private void setActiveMarker(int position) {
        Marker activeMarker = null;

        if (pointMarkers != null) {

            if (pointMarkers.getItems().size() != 0) {
                try {
                    if (oldPosition > -1) {
                        if (!isHikeDownloaded) {

                            if (pointsList.get(oldPosition).getReviewVersionFlag()) {
                                pointMarkers.getItem(oldPosition).setIcon(ImageHelper.drawTextToBitmap(getActivity(), R.drawable.pointsolid, "" + (oldPosition + 1)));
                                pointMarkers.getItem(oldPosition).setAlpha(POINT_ALPHA);
                                pointMarkers.getItem(oldPosition).setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                            } else {
                                pointMarkers.getItem(oldPosition).setIcon(getActivity().getResources().getDrawable(R.drawable.ic_to_download));
                                pointMarkers.getItem(oldPosition).setAlpha(POINT_ALPHA);
                                pointMarkers.getItem(oldPosition).setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                            }
                        } else {
                            pointMarkers.getItem(oldPosition).setIcon(ImageHelper.drawTextToBitmap(getActivity(), R.drawable.pointsolid, "" + (oldPosition + 1)));
                            pointMarkers.getItem(oldPosition).setAlpha(POINT_ALPHA);
                            pointMarkers.getItem(oldPosition).setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        }
                    }

                    if (position >= pointMarkers.getItems().size()) {
                        activeMarker = pointMarkers.getItem(0);
                        startPoint = activeMarker.getPosition();
                    } else {
                        activeMarker = pointMarkers.getItem(position);
                        startPoint = activeMarker.getPosition();
                    }
                    activeMarker.setIcon(getActivity().getResources().getDrawable(R.drawable.one));
                    activeMarker.setAlpha(0f);


                    GeoPoint dummyGeoPoint = new GeoPoint(activeMarker.getPosition());
                    Drawable overlayDrawable = ImageHelper.drawTextToBitmap(getActivity(), R.drawable.this_point_solid, "" + (position + 1));

                    ArrayList<OverlayItem> items = new ArrayList<>();

                    if (map.getOverlays() != null && overlayIcon != null) {
                        map.getOverlays().remove(overlayIcon);
                    }

                    overlayItem = new OverlayItem("title", "description", dummyGeoPoint);
                    overlayItem.setMarker(overlayDrawable);

                    items.add(overlayItem);
                    overlayIcon = new ItemizedIconOverlay<>(items, null, new DefaultResourceProxyImpl(getActivity().getBaseContext()));
                    map.getOverlays().add(overlayIcon);

                    alphaDirection = true;
                    currentOverlayAlpha = 0;

                    if (timer == null) {
                        timer = new Timer();
                    }

                    if (runnable == null) {
                        runnable = new Runnable() {

                            @Override
                            public void run() {
                                if (currentOverlayAlpha > 255) {
                                    currentOverlayAlpha = 255;
                                    alphaDirection = !alphaDirection;
                                } else if (currentOverlayAlpha < 0) {
                                    currentOverlayAlpha = 0;
                                    alphaDirection = !alphaDirection;
                                }

                                // first remove the current overlay icon from map view
                                map.getOverlays().remove(overlayIcon);
                                map.invalidate();

                                // set the new alpha to the overlayDrawable
                                Drawable overlayDrawable = overlayItem.getDrawable();
                                overlayDrawable.setAlpha(currentOverlayAlpha);
                                overlayItem.setMarker(overlayDrawable);

                                // add the overlay item with the new alpha to map view
                                overlayIcon.removeAllItems();
                                overlayIcon.addItem(overlayItem);
                                map.getOverlays().add(overlayIcon);

                                if (alphaDirection) {
                                    currentOverlayAlpha += PULSE_INTERVAL;
                                } else {
                                    currentOverlayAlpha -= PULSE_INTERVAL;
                                }
                            }
                        };
                    }
                    if (timerTask == null) {
                        timerTask = new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(runnable);
                                }
                            }
                        };
                    }

                    timer.schedule(timerTask, 100, 100);


                    map.getController().animateTo(activeMarker.getPosition());
                    oldPosition = position;
                } catch (Exception e) {
                    if (activeMarker != null) {
                        map.getController().animateTo(activeMarker.getPosition());
                    }
                    oldPosition = position;
                }
            }

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!this.isDetached()) {
            if (myLocationOverlay.getLocation() != null) {
                outState.putParcelable("location", myLocationOverlay.getLocation());
            }
            outState.putFloat("MAP_CENTER_LAT", (float) map.getMapCenter().getLatitude());
            outState.putFloat("MAP_CENTER_LON", (float) map.getMapCenter().getLongitude());
            outState.putInt("zoom", map.getZoomLevel());
            saveMapPrefs();
        }
    }

    private void saveMapPrefs() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt("MAP_ZOOM_LEVEL", map.getZoomLevel());
        edit.putFloat("MAP_CENTER_LAT", (float) map.getMapCenter().getLatitude());
        edit.putFloat("MAP_CENTER_LON", (float) map.getMapCenter().getLongitude());

        MapTileProviderBase tileProvider = map.getTileProvider();
        String tileProviderName = tileProvider.getTileSource().name();
        edit.putString("TILE_PROVIDER", tileProviderName);
        edit.putInt("ROUTE_PROVIDER", mWhichRouteProvider);
        edit.apply();
        saveHikePrefs();
    }

    private void saveHikePrefs() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(HIKE_ID, hikeId);
        edit.putInt(POINT_POSITION, pointPosition);
        edit.putInt(Constants.HIKE_SOURCE, hikeSource);
        edit.apply();
    }

    @Override
    public void onStop() {
        super.onStop();
        saveHikePrefs();
        if (!this.isDetached())
            saveMapPrefs();
        HikeInfoWindow.setNullInfoWindow();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_map, menu);
        if (trackShown) animateView.startAnimation(animation);
        menu.findItem(R.id.menu_track).setActionView(animateView);
        if (map.getTileProvider().getTileSource() == TileSourceFactory.MAPNIK) {
            menu.findItem(R.id.menu_tile_mapnik).setChecked(true);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_tile_mapnik: {
                map.setTileSource(TileSourceFactory.MAPNIK);
                item.setChecked(true);
                return true;
            }
            case R.id.menu_tile_mapbox_satellite: {
                setMapBoxSateliteTileSource();
                item.setChecked(true);
                return true;
            }
            case R.id.menu_download_view_area:
                downloadTiles(map);
                return true;
            case R.id.menu_clear_view_area:
                clearTiles(map);
                return true;
            case R.id.menu_cache_usage:
                showDeleteDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void setMapBoxSateliteTileSource() {
        final MapBoxTileSource tileSource = new MapBoxTileSource();
        tileSource.retrieveAccessToken(getActivity());
        tileSource.retrieveMapBoxMapId(getActivity());
        map.setTileSource(tileSource);
    }

    private void showDeleteDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.menu_clear_all_tiles_qu));
        dialog.setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> clearAllTiles());
        dialog.setNegativeButton(getString(R.string.no), (dialogInterface, i) -> dialogInterface.dismiss());
        dialog.setCancelable(true);
        dialog.show();
    }

    private void clearAllTiles() {
        File filedirMapnik = new File(Environment.getExternalStorageDirectory() + "/osmdroid/tiles/Mapnik");
        File filedirMapBoxSatellite = new File(Environment.getExternalStorageDirectory() + "/osmdroid/tiles/MapBoxSatelliteLabelled");
        DeleteRecursive(filedirMapnik);
        DeleteRecursive(filedirMapBoxSatellite);
    }

    void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles()) {
                DeleteRecursive(child);
            }
        fileOrDirectory.delete();
    }

    private boolean isGpsEnabled() {
        if (mLocationManager == null) {
            return false;
        } else
            return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void showWidgets() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            if (!((AppCompatActivity) getActivity()).getSupportActionBar().isShowing()) {
                ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
            }
            ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        }
        if (btnMapMode != null) {
            btnMapMode.setVisibility(View.VISIBLE);
        }
        if (imgbtnLocation != null) {
            imgbtnLocation.setVisibility(View.VISIBLE);
        }
        if (zoomTV != null) {
            zoomTV.setVisibility(View.VISIBLE);
        }
        if (btnPlus != null) {
            btnPlus.setVisibility(View.VISIBLE);
        }
        if (btnMinus != null) {
            btnMinus.setVisibility(View.VISIBLE);
        }
    }

    private void hideWidgets(int miliSeconds) {
        if (hidingDelay != null) {
            handler.removeCallbacks(hidingDelay);
        } else {
            hidingDelay = new Runnable() {
                @Override
                public void run() {
                    if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
                        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
                    }
                    if (btnMapMode != null) {
                        btnMapMode.setVisibility(View.GONE);
                    }
                    if (imgbtnLocation != null) {
                        imgbtnLocation.setVisibility(View.GONE);
                    }
                    if (zoomTV != null) {
                        zoomTV.setVisibility(View.GONE);
                    }
                    if (btnPlus != null) {
                        btnPlus.setVisibility(View.GONE);
                    }
                    if (btnMinus != null) {
                        btnMinus.setVisibility(View.GONE);
                    }
                }
            };
        }
        handler.postDelayed(hidingDelay, miliSeconds);
    }

    private void hideWidgets() {
        hideWidgets(Constants.WAIT_SECOND_TO_HIDE_WiDGETS * 1000);
    }

    private void stopHiding() {
        if (hidingDelay != null) {
            handler.removeCallbacks(hidingDelay);
        }
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            if (!((AppCompatActivity) getActivity()).getSupportActionBar().isShowing()) {
                ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
            }
            ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        }
        if (animateView != null) {
            animateView.clearAnimation();
        }
    }

    private ImageView getAnimateView() {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.alpha_track, null);
        final ImageView iv = view.findViewById(R.id.iv_alpha_track);

        iv.setOnClickListener(view1 -> {
            if (!trackShown) {
                if (isGpsEnabled()) {
                    if (track == null) {
                        track = new ArrayList<>();
                    }
                    if (myLocationOverlay.getLocation() != null) {
                        track.add(myLocationOverlay.getLocation());
                        map.getController().animateTo(myLocationOverlay.getLocation());
                    }
                    iv.startAnimation(animation);
                    trackShown = true;
                    trekOverlay();
                    startLocationUpdates();
                } else {
                    trackShown = false;
                    Toast.makeText(getActivity(), getString(R.string.location_service_unavailable), Toast.LENGTH_LONG).show();
                }
            } else {
                iv.clearAnimation();
                trekOverlayClean();
                trackShown = false;
                track = null;
            }
        });
        return iv;
    }

    public void trekOverlay() {
        if (track != null) {
            if (track.size() > 0) {
                trekOverlayClean();
                lineTrek = new Polyline(getContext());
                lineTrek.setPoints(track);
                lineTrek.setColor(getResources().getColor(R.color.blue_transparent));
                lineTrek.setWidth(4.0f);
                map.getOverlays().add(lineTrek);
                map.invalidate();
            }
        }
    }

    public void trekOverlayClean() {
        if (lineTrek != null) map.getOverlays().remove(lineTrek);
        map.invalidate();
    }

    private void downloadTiles(MapView map) {
        CacheManager cacheManager = new CacheManager(map);
        int zoomMin = map.getZoomLevel();
        int zoomMax = map.getZoomLevel() + 4;
        cacheManager.downloadAreaAsync(getActivity(), map.getBoundingBox(), zoomMin, zoomMax);

    }

    private void clearTiles(MapView map) {
        CacheManager cacheManager = new CacheManager(map);
        int zoomMin = map.getZoomLevel();
        int zoomMax = map.getZoomLevel() + 4;
        cacheManager.cleanAreaAsync(getActivity(), map.getBoundingBox(), zoomMin, zoomMax);
    }

    private void startLocationUpdates() {
        for (final String provider : mLocationManager.getProviders(true)) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mLocationManager.requestLocationUpdates(provider, 30 * 1000, 1.0f, this);
        }
    }

    @Override
    public void onLocationChanged(Location pLoc) {
        long currentTime = System.currentTimeMillis();
        if (new NetworkLocationIgnorer().shouldIgnore(pLoc.getProvider(), currentTime))
            return;
        double dT = currentTime - mLastTime;
        if (dT < 100.0) {
            return;
        }
        mLastTime = currentTime;

        GeoPoint newLocation = new GeoPoint(pLoc);

        if (!myLocationOverlay.isEnabled()) {
            myLocationOverlay.setEnabled(true);
            map.getController().animateTo(newLocation);
        }

        GeoPoint prevLocation = myLocationOverlay.getLocation();
        myLocationOverlay.setLocation(newLocation);
        myLocationOverlay.setAccuracy((int) pLoc.getAccuracy());

        if (trackShown) {
            track.add(myLocationOverlay.getLocation());
            trekOverlay();
        }


        if (prevLocation != null && pLoc.getProvider().equals(LocationManager.GPS_PROVIDER)) {

            mAzimuthAngleSpeed = (float) prevLocation.bearingTo(newLocation);

            if (mAzimuthAngleSpeed > 0.1) {

                myLocationOverlay.setBearing(mAzimuthAngleSpeed);

                if (mTrackingMode) {
                    map.getController().animateTo(newLocation);
                    map.setMapOrientation(-mAzimuthAngleSpeed);
                } else {
                    map.invalidate();
                }
            }
        }
    }

    public void clearHikesOverlay() {
        if (hikesMarks != null) {
            hikesMarks.getItem(0).setInfoWindow(null);
            map.getOverlays().remove(hikesMarks);
        }
        if (myHikesMarks != null) {
            myHikesMarks.getItem(0).setInfoWindow(null);
            map.getOverlays().remove(myHikesMarks);
        }
        if (pointMarkers != null) {
            map.getOverlays().remove(pointMarkers);
        }
        map.invalidate();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onResume() {
        super.onResume();
        hideWidgets();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopHiding();
        saveMapPrefs();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null)
            getActivity().unregisterReceiver(broadcastReceiver);
    }
}


