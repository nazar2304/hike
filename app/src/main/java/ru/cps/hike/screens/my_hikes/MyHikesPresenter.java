package ru.cps.hike.screens.my_hikes;

import android.os.AsyncTask;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.cps.hike.App;
import ru.cps.hike.base.BasePresenter;
import ru.cps.hike.connection.Api;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.dagger.module.LocalStorage;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;

@InjectViewState
public class MyHikesPresenter extends BasePresenter<MyHikesView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;
    private List<Hike> hikes = new ArrayList<>();

    MyHikesPresenter() {
        App.getAppComponent().inject(this);
    }

    void load() {
        new GetDataAsync().execute();
    }

    public List<Hike> getHikes() {
        return hikes;
    }

    private class GetDataAsync extends AsyncTask<String, Void, List<Hike>> {

        @Override
        protected List<Hike> doInBackground(String... strings) {
            List<Hike> hikesList = new ArrayList<>();
            ArrayList<String> hikeNames = SaveInSharedPref.loadHikeIds();
            if (hikeNames != null) {
                for (String hikeName : hikeNames) {
                    String jsonHike = FileTools.readExportFile(hikeName);
                    if (!jsonHike.isEmpty()) {
                        Hike hike = new Gson().fromJson(jsonHike, Hike.class);
                        Map<String, String> query = new HashMap<>();
                        query.put("lang", App.getCurrentLang());
                        String size = FileTools.readExportFileMyHike(hikeName);
                        Hike hikeSize = new Gson().fromJson(size, Hike.class);
                        if (hikeSize == null || hikeSize.getSize() == 0) {
                            Disposable disposable = mApi.getHikeSize(hike.getId(), query)
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(result -> {
                                                hike.setSize(result.getSize());
                                                getViewState().notifyList();
                                            }
                                            , throwable -> {
                                                Log.w(TAG, "doInBackground: " + throwable.getMessage());
                                            });
                            unsubscribeOnDestroy(disposable);
                        } else {
                            hike.setSize(hikeSize.getSize());
                        }

                        hikesList.add(hike);
                    }
                }
            }
            return hikesList;
        }

        @Override
        protected void onPostExecute(List<Hike> hikesList) {
            super.onPostExecute(hikesList);
            hikes.clear();
            hikes.addAll(hikesList);
            getViewState().notifyList();
        }
    }
}
