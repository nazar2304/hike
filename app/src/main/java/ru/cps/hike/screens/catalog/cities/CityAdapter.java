package ru.cps.hike.screens.catalog.cities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.model.City;
import ru.cps.hike.Constants;


class CityAdapter extends BaseAdapter {
    private LayoutInflater lInflater;
    private List<City> cityList;
    private DisplayImageOptions options;

    CityAdapter(Context context, List<City> cities) {
        cityList = cities;
        if (context != null) {
            lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.no_image)
                .showImageOnFail(R.drawable.no_image)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder holder;
        View view = convertView;
        final City c = getCity(position);
        if (c != null) {
            if (view == null) {
                view = lInflater.inflate(R.layout.city_item, parent, false);

                holder = new ViewHolder();

                holder.imageView = view.findViewById(R.id.iv_city_image);
                holder.tvCityName = view.findViewById(R.id.tv_city_name);
                holder.tvHikesCount = view.findViewById(R.id.tv_hikes_number);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.tvCityName.setText(c.getLang().getName());
            holder.tvHikesCount.setText(String.valueOf(c.getHikesCount()));

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.imageView.getLayoutParams();
            params.height = Constants.getCatalogItemImageHeight();
            holder.imageView.setLayoutParams(params);
            ImageLoader.getInstance().displayImage(App.getApp().getResources().getString(R.string.general) + c.getImage().getPreview(), holder.imageView, options);
        }

        return view;
    }

    City getCity(int position) {
        return ((City) getItem(position));
    }

    @Override
    public int getCount() {
        if (cityList != null) {
            return cityList.size();
        } else return 0;
    }

    @Override
    public Object getItem(int position) {
        return cityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        ImageView imageView;
        TextView tvCityName;
        TextView tvHikesCount;
    }
}
