package ru.cps.hike.screens.catalog.country;

import ru.cps.hike.base.BaseView;

public interface CountriesView extends BaseView {
    void notifyList();

    void showEmpty(boolean status);
}
