package ru.cps.hike.screens.catalog.country;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Country;


class CountryAdapter extends BaseAdapter {

    private List<Country> countryList;
    private DisplayImageOptions options;
    private LayoutInflater lInflater;

    CountryAdapter(Context context, List<Country> countries) {
        countryList = countries;
        if (context != null) {
            lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.no_image)
                .showImageOnFail(R.drawable.no_image)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;

        if (countryList == null || countryList.size() == 0) {
            view = lInflater.inflate(R.layout.country_empty_item, parent, false);
            return view;
        }

        final Country c = getCountry(position);

        if (view == null) {
            view = lInflater.inflate(R.layout.country_item, parent, false);
            holder = new ViewHolder();
            holder.imageView = view.findViewById(R.id.iv_country_flag);
            holder.tvCountryName = view.findViewById(R.id.tv_country_name);
            holder.tvHikesCount = view.findViewById(R.id.tv_hikes_number);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tvCountryName.setText(c.getLang().getName());
        holder.tvHikesCount.setText(String.valueOf(c.getCitiesCount()));

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.imageView.getLayoutParams();
        params.height = Constants.getCatalogItemImageHeight();
        holder.imageView.setLayoutParams(params);
        ImageLoader.getInstance().displayImage(App.getApp().getResources().getString(R.string.general) + c.getImage().getPreview(), holder.imageView, options);
        return view;
    }

    Country getCountry(int position) {
        return ((Country) getItem(position));
    }

    @Override
    public int getCount() {
       return countryList.size();
    }

    @Override
    public Object getItem(int position) {
        return countryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        ImageView imageView;
        TextView tvCountryName;
        TextView tvHikesCount;
    }
}
