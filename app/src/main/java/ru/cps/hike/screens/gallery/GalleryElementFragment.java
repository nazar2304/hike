package ru.cps.hike.screens.gallery;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ortiz.touch.TouchImageView;

import java.io.File;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Image;


public class GalleryElementFragment extends Fragment {

    private  static final String IMAGE_NAME = "imageName";
    private  static final String FULL_IMAGE_PATH = "fullImagePath";
    private  static final String PREVIEW_IMAGE_PATH = "previewImagePath";

    private String imageName;
    private String fullImagePath;
    private String hikeId;
    private DisplayImageOptions options;

    public static GalleryElementFragment newInstance(Image image, String hikeId) {
        Bundle args = new Bundle();
        args.putSerializable(IMAGE_NAME, image.getTitle());
        args.putSerializable(FULL_IMAGE_PATH, image.getFull());
        args.putSerializable(PREVIEW_IMAGE_PATH, image.getPreview());
        args.putString(Constants.ID, hikeId);

        GalleryElementFragment fragment = new GalleryElementFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imageName = getArguments().getString(IMAGE_NAME);
            fullImagePath = getArguments().getString(FULL_IMAGE_PATH);
            hikeId = getArguments().getString(Constants.ID);
        }

        options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gallery_element, parent, false);

        TextView tvImageTitle = v.findViewById(R.id.tv_galary_element_name);
        tvImageTitle.setText(imageName);

        TouchImageView ivImage = v.findViewById(R.id.iv_galary_image);
        if (SaveInSharedPref.isHikeDownloaded(hikeId)) {
            String sb = "file:///" + FileTools.getFolder() + "/" + hikeId + "/" + new File(fullImagePath).getName();
            ImageLoader.getInstance().displayImage(sb, ivImage, options);
        } else {
            ImageLoader.getInstance().displayImage(App.getApp().getResources().getString(R.string.general) + fullImagePath, ivImage, options);
        }
        return v;
    }
}
