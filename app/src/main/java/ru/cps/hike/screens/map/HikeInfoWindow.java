package ru.cps.hike.screens.map;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import org.osmdroid.bonuspack.overlays.BasicInfoWindow;
import org.osmdroid.views.MapView;

import ru.cps.hike.App;
import ru.cps.hike.R;


public class HikeInfoWindow extends BasicInfoWindow {
    private static HikeInfoWindow instance;
    private PointMarker hikeMarker;


    private HikeInfoWindow(int layoutId, MapView mapView) {
        super(layoutId, mapView);

        this.mView.setOnTouchListener((v, e) -> {
            if (e.getAction() == 1) {
                HikeInfoWindow.this.close();
                Intent intent = new Intent(MapFragment.BROADCAST_ACTION_HIKE_ID);
                intent.putExtra(MapFragment.HIKE_ID, hikeMarker.getId());
                App.getApp().sendBroadcast(intent);
            }

            return true;
        });
    }

    public static HikeInfoWindow getInstance(MapView mapView) {
        if (instance == null) {
            instance = new HikeInfoWindow(R.layout.bonuspack_bubble, mapView);
        }
        return instance;
    }

    static void setNullInfoWindow() {
        instance = null;
    }

    public void onOpen(Object item) {
        super.onOpen(item);
        this.hikeMarker = (PointMarker) item;
        ImageView imageView = this.mView.findViewById(R.id.bubble_image);
        Drawable image = this.hikeMarker.getImage();
        if (image != null && imageView != null) {
            imageView.setImageDrawable(image);
            imageView.setVisibility(View.VISIBLE);
        } else if(imageView != null) {
            imageView.setVisibility(View.INVISIBLE);
        }
    }

    public void onClose() {
        super.onClose();
    }

}
