package ru.cps.hike.screens.catalog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.cps.hike.R;
import ru.cps.hike.screens.catalog.country.CountriesFragment;


public class TabContainerFragment extends BaseContainerFragment {
    private boolean mIsViewInited;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.container_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!mIsViewInited) {
            mIsViewInited = true;
            initView();
        }
    }

    private void initView() {
        CountriesFragment catalogFragment = new CountriesFragment();
        catalogFragment.setTabTag(getTag());
        replaceFragment(catalogFragment, false);
    }

}
