package ru.cps.hike.screens.catalog.hikes;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.http.StringRequests;
import ru.cps.hike.Constants;
import ru.cps.hike.model.Hike;
import ru.cps.hike.views.ButtonCangeStyle;


class HikeAdapter extends BaseAdapter {
    private LayoutInflater lInflater;
    private List<Hike> hikesList;
    private SharedPreferences prefs;

    private DisplayImageOptions options;

    HikeAdapter(Context context, List<Hike> hikes, String cityId) {
        hikesList = hikes;
        if (context != null) {
            lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.no_image)
                    .showImageOnFail(R.drawable.no_image)
                    .resetViewBeforeLoading(true)
                    .cacheOnDisk(true)
                    .build();
        }
        if (context != null)
            this.prefs = context.getSharedPreferences(cityId, Context.MODE_PRIVATE);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        final Hike h = getHike(position);

        if (view == null) {
            view = lInflater.inflate(R.layout.hike_item, parent, false);

            holder = new ViewHolder();

            holder.imageView = view.findViewById(R.id.iv_hike_image);
            holder.tvHikeName = view.findViewById(R.id.tv_hike_name);
            holder.spinner = view.findViewById(R.id.pb_loading);
            holder.btnDownload = view.findViewById(R.id.btn_download_hike);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String sb = h.getLang().getTitle() + "\n" + "\n" + holder.imageView.getResources().getString(R.string.points) + " " + h.getPointsCount() + "\n" + holder.imageView.getResources().getString(R.string.size) + "  " + h.getSize() + " " + holder.imageView.getResources().getString(R.string.mb);
        holder.tvHikeName.setText(sb);

        if (App.downloadList.contains(h.getId())) {
            ButtonCangeStyle.setButtonDownloading(holder.btnDownload);
        } else {
            if (FileTools.checkIsSaveHike(h.getId()) && prefs.getInt(h.getId(), Constants.DOWNLOAD_START) == Constants.DOWNLOAD_COMPLETE) {
                ButtonCangeStyle.setButtonDownloadComplete(holder.btnDownload, h.getId(), Constants.MAP_START_FROM_TAB, Constants.HIKE_FROM_CATALOG);
            } else {
                ButtonCangeStyle.setButtonDownloadStart(holder.btnDownload, h, view.getContext());
            }
        }

        if (!StringRequests.isOnline()) {
            ButtonCangeStyle.setButtonDownloadNoConnection(holder.btnDownload);
        }

        String imgName = h.getImage().getPreview();

        ImageLoader.getInstance().displayImage(App.getApp().getResources().getString(R.string.general) + imgName, holder.imageView, options, new AnimateFirstDisplayListener(holder));
        return view;
    }

    Hike getHike(int position) {
        return ((Hike) getItem(position));
    }

    @Override
    public int getCount() {
        return hikesList.size();
    }

    @Override
    public Object getItem(int position) {
        return hikesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        ImageView imageView;
        TextView tvHikeName;
        ProgressBar spinner;
        Button btnDownload;
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<>());
        ViewHolder holder;

        AnimateFirstDisplayListener(ViewHolder viewHolder) {
            holder = viewHolder;
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {
            holder.spinner.setVisibility(View.VISIBLE);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    displayedImages.add(imageUri);
                }
            }
            holder.spinner.setVisibility(View.GONE);
        }

    }
}
