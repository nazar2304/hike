package ru.cps.hike.screens.map;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

import java.util.List;


public class GenericMapView extends FrameLayout {
    private MapView mMapView;

    public GenericMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setTileProvider(MapTileProviderBase aTileProvider) {
        if (mMapView != null) {
            this.removeView(mMapView);
        }
        ResourceProxy resourceProxy = new DefaultResourceProxyImpl(this.getContext());
        MapView newMapView = new MapView(this.getContext(), resourceProxy, aTileProvider);
        if (mMapView != null) {
            IMapController mapController = newMapView.getController();
            mapController.setZoom(mMapView.getZoomLevel());
            mapController.setCenter(mMapView.getMapCenter());
            newMapView.setBuiltInZoomControls(true);
            newMapView.setMultiTouchControls(true);
            newMapView.setUseDataConnection(mMapView.useDataConnection());
            newMapView.setMapOrientation(mMapView.getMapOrientation());
            newMapView.setScrollableAreaLimit(mMapView.getScrollableAreaLimit());
            List<Overlay> overlays = mMapView.getOverlays();
            for (Overlay o : overlays)
                newMapView.getOverlays().add(o);
        }

        mMapView = newMapView;
        this.addView(mMapView);
    }

    public MapView getMapView() {
        return mMapView;
    }


}
