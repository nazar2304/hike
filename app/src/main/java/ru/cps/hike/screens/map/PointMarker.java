package ru.cps.hike.screens.map;

import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.views.MapView;


public class PointMarker extends Marker {
    private String id;

    PointMarker(MapView mapView) {
        super(mapView);
        this.setInfoWindow(HikeInfoWindow.getInstance(mapView));
    }

    @Override
    protected boolean onMarkerClickDefault(Marker marker, MapView mapView) {
        return super.onMarkerClickDefault(marker, mapView);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
