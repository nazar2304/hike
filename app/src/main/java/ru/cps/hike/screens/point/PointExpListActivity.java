package ru.cps.hike.screens.point;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.cps.hike.App;
import ru.cps.hike.R;
import ru.cps.hike.base.BaseActivity;
import ru.cps.hike.controller.audio.AudioService;
import ru.cps.hike.controller.dowload_hike_listener.DownloadHikeDispatcher;
import ru.cps.hike.controller.dowload_hike_listener.IDownloadStatusListener;
import ru.cps.hike.controller.files.FileTools;
import ru.cps.hike.controller.files.SaveInSharedPref;
import ru.cps.hike.Constants;
import ru.cps.hike.views.ButtonCangeStyle;


public class PointExpListActivity extends BaseActivity implements IDownloadStatusListener, PointExpListView {

    @InjectPresenter PointExpListPresenter mPresenter;
    @BindView(R.id.elvMain) ExpandableListView elvMain;
    @BindView(R.id.btn_download_hike) Button btnDownload;
    private NewPointExpListAdapter pointExpListAdapter;

    SharedPreferences prefs;
    Menu myMenu;
    boolean toUpdateList = true;
    private ProgressDialog pdBuff = null;

    BroadcastReceiver onLineReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo netInfo = cm.getActiveNetworkInfo();

                if (netInfo != null) {
                    boolean isConn = netInfo.isConnectedOrConnecting();

                    if (isConn) {
                        loadHikeStatus();
                        if (toUpdateList) {
                            mPresenter.updateList();
                            toUpdateList = false;
                        }
                    } else {
                        ButtonCangeStyle.setButtonDownloadNoConnection(btnDownload);
                    }
                } else {
                    ButtonCangeStyle.setButtonDownloadNoConnection(btnDownload);
                }
            }
        }
    };
    BroadcastReceiver stopPlayerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            pointExpListAdapter.notifyDataSetChanged();
        }
    };
    private BroadcastReceiver broadcastBufferReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showPD(intent);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exp_list);
        unbinder = ButterKnife.bind(this);

        IntentFilter onLineFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(onLineReceiver, onLineFilter);
        IntentFilter stopPlayFilter = new IntentFilter(Constants.BROADCAST_PLAYER_STATUS);
        registerReceiver(stopPlayerReceiver, stopPlayFilter);

        DownloadHikeDispatcher.addListener(this);

        elvMain.setOnGroupClickListener((expandableListView, view, i, l) -> i == 0);
        elvMain.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {

                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    elvMain.collapseGroup(previousGroup);
                }
                previousGroup = groupPosition;
            }
        });
        mPresenter.setIndex(getIntent().getIntExtra(Constants.INDEX, -1));
        mPresenter.setHikeId(getIntent().getStringExtra(Constants.ID));
        mPresenter.setPointId(getIntent().getStringExtra(Constants.POINT_ID));
        mPresenter.loadHike();

        pointExpListAdapter = new NewPointExpListAdapter(PointExpListActivity.this, mPresenter.getHike(), getIntent().getBooleanExtra(Constants.PLAY, false));
        elvMain.setAdapter(pointExpListAdapter);


    //    mPresenter.updateList();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mPresenter.getHike().getLang().getTitle());
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        prefs = getSharedPreferences(mPresenter.getHike().getCityId(), Context.MODE_PRIVATE);

        loadHikeStatus();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastBufferReceiver, new IntentFilter(Constants.BROADCAST_BUFFERING));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(broadcastBufferReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ImageLoader.getInstance().clearMemoryCache();
        unregisterReceiver(onLineReceiver);
        unregisterReceiver(stopPlayerReceiver);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        int width = Constants.screenWidth;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            elvMain.setIndicatorBounds(width - (int) (55 * App.getApp().getDp() + 0.5f), width - (int) (55 * App.getApp().getDp() + 0.5f));
        } else {
            elvMain.setIndicatorBoundsRelative(width - (int) (55 * App.getApp().getDp() + 0.5f), width - (int) (55 * App.getApp().getDp() + 0.5f));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        myMenu = menu;
        getMenuInflater().inflate(R.menu.menu_hike_activity, menu);
        if (btnDownload.getVisibility() == View.VISIBLE) {
            hideDelButton(menu);
        } else {
            showDelButton(menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.menu_delete:
                showDeleteDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDeleteDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.delete_hike));
        dialog.setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> deleteHike());
        dialog.setNegativeButton(getString(R.string.no), (dialogInterface, i) -> dialogInterface.dismiss());
        dialog.setCancelable(true);
        dialog.show();
    }

    private void hideDelButton(Menu menu) {
        if (menu != null) {
            menu.getItem(0).setVisible(false);
            invalidateOptionsMenu();
        }
    }

    private void showDelButton(Menu menu) {
        if (menu != null) {
            menu.getItem(0).setVisible(true);
            invalidateOptionsMenu();
        }
    }

    private void deleteHike() {
        prefs.edit().remove(mPresenter.getHikeId()).apply();
        SaveInSharedPref.removeHikeId(mPresenter.getHikeId());
        final String folderToDelete = FileTools.getFolder() + "/" + mPresenter.getHikeId();
        Thread myThread = new Thread(() -> DeleteRecursive(new File(folderToDelete)));
        myThread.start();

        Constants.toUpdateMyHikesFragment = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.parse(folderToDelete);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse(folderToDelete + Environment.getExternalStorageDirectory())));
        }
        finish();
    }

    void DeleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles()) {
                DeleteRecursive(child);
            }
        if (!fileOrDirectory.getName().equals("export.wlk"))
            fileOrDirectory.delete();
    }

    @Override
    public void onDownloadComplete(int resCode, String gotHikeId) {
        if (gotHikeId.equals(mPresenter.getHikeId())) {
            loadHikeStatus();
        }
    }

    void loadHikeStatus() {
        if (btnDownload == null) {
            return;
        }
        if (App.downloadList.contains(mPresenter.getHikeId())) {
            btnDownload.setVisibility(View.VISIBLE);
            ButtonCangeStyle.setButtonDownloading(btnDownload);

        } else {
            if (FileTools.checkIsSaveHike(mPresenter.getHikeId()) && prefs.getInt(mPresenter.getHike().getId(), Constants.DOWNLOAD_START) == Constants.DOWNLOAD_COMPLETE) {
                btnDownload.setVisibility(View.GONE);
                showDelButton(myMenu);
                pointExpListAdapter.notifyDataSetChanged();

            } else {
                btnDownload.setVisibility(View.VISIBLE);
                ButtonCangeStyle.setButtonDownloadStart(btnDownload, mPresenter.getHike(), this);
                hideDelButton(myMenu);

            }
        }
    }

    private void showPD(Intent bufferIntent) {
        String bufferValue = bufferIntent.getStringExtra("buffering");
        int bufferIntValue = Integer.parseInt(bufferValue);
        switch (bufferIntValue) {
            case 0:
                if (pdBuff != null) {
                    pdBuff.dismiss();
                }
                break;
            case 1:
                bufferDialogue();
                break;
        }
    }

    private void bufferDialogue() {
        if (pdBuff == null)
            pdBuff = ProgressDialog.show(this, getString(R.string.buffering), getString(R.string.acquiring), true);
        else
            pdBuff.show();
        pdBuff.setCancelable(true);
        pdBuff.setOnCancelListener(dialogInterface -> stopService(new Intent(this, AudioService.class)));
    }

    @Override
    public void notifyList() {
        pointExpListAdapter.notifyDataSetChanged();
    }

    @Override
    public void openGroup(int index) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                elvMain.expandGroup(index);
                elvMain.setSelectedGroup(index);
            }
        }, 20);
    }


}
