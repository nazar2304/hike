package ru.cps.hike.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import ru.cps.hike.controller.dowload_hike_listener.DownloadControlListener;

public class NotificationDismissedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String s = intent.getStringExtra("hike");
        DownloadControlListener.cancel(s);
    }
}
