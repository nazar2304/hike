package ru.cps.hike.base;

import android.annotation.SuppressLint;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;

import butterknife.Unbinder;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseView {

    public static final String TAG = "myLog";

    protected View progress;
    protected Unbinder unbinder;

    @SuppressLint("ResourceType")
    public void showToast(@IdRes int id) {
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(boolean status) {
        progress.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }
}
