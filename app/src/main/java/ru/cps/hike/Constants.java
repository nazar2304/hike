package ru.cps.hike;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

public class Constants {
    public static final String IMAGE_FOLDER = "image";
    public static final int WAIT_SECOND_TO_HIDE_WiDGETS = 30;
    public static final String HIKE_SOURCE = "HIKE_SOURCE";
    public static final int HIKE_FROM_CATALOG = 0;
    public static final int HIKE_DOWNLOADED = 1;
    public static final String COUNTRY = "COUNTRY";
    public static final String CITY = "CITY";
    public static final String JSON_EXT = ".wlk";
    public static final String COUNTRIES_FILE_NAME = "cntr";
    public static final String VISIBILITY = "VISIBILITY";
    public static final String PLAY = "play";
    public static final String BROADCAST_NEXT_TRACK = "ru.cps.player.next_track";
    public static final String BROADCAST_PLAYER_STATUS = "ru.cps.player.status";
    public static final String BROADCAST_DOWNLOAD = "ru.cps.player.download";
    public static final String BROADCAST_SEEKBAR = "ru.cps.hike.broadcastseekbar";
    public static final String BROADCAST_BUFFERING = "ru.cps.hike.broadcastbuffering";
    public static final String DOWNLOAD = "download";
    public static final int DOWNLOADING = 11;
    public static final int DOWNLOAD_COMPLETE = 13;
    public static final int DOWNLOAD_START = 15;

    // inner data transfer
    public static final String DOWNLOADED_HIKES_ARRAY = "DOWNLOADED_HIKES_ARRAY";
    public static final int DOWNLOAD_ERROR = -1;
    public static final String IS_DOWNLOADED = "DOWNLOADED";
    public static final int MAP_START_FROM_TAB = 13;
    public static final String MAP_START = "MAP_START";
    //   save data
    public static final String ID = "id";
    public static final String HIKE_ID = "hikeId";
    public static final String CITY_ID = "cityId";
    public static final String COUNTRY_ID = "countryId";
    public static final String IMAGE = "image";
    public static final String IMAGES = "images";

    //    public static final String BROADCAST_UPDATE_UI = "ru.cps.hike.broadcastupdateui";
    public static final String POINT_ID = "pointId";
    public static final String TITLE = "title";
    public static final String AUDIO = "audio";
    public static final String INDEX = "index";
    public static final String X = "x";
    public static final String Y = "y";
    //    requests parse const
    public final static String SHORT_POINTS_REQUEST = "/api/v2/points/?hike=";
    public final static String NEW_DOWNLOAD_RQUEST_BEGIN = "/api/v2/hikes/";
    public final static String DOWNLOAD_RQUEST_END = "/download";
    public final static String DERMO_LANG = "&lang=";
    public final static String DERMO_LANG_QU = "?lang=";

    public final static String HIKE_CHANEL_ID = "hike_chanel_id";
    // -------- almost const
    public static boolean toPlay;
    public static boolean playerPaused;
    public static String playingHikeId = "";
    public static boolean toUpdateMyHikesFragment = false;
    public static int playIndex;

    public static int screenWidth;
    public static int screenHeight;
    //    parser constants

    public static int getCatalogItemImageHeight() {
        WindowManager windowManager = (WindowManager) App.getApp().getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return new Double(0.58 * size.x).intValue();
    }
}
